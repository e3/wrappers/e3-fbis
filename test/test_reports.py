import pytest
import os
import xml.etree.ElementTree as ET
import re


@pytest.fixture
def parsed_xml():
    base_dir = os.getcwd()
    xml_path = os.path.join(base_dir, "test", "report.xml")
    tree = ET.parse(xml_path)
    return tree


@pytest.fixture
def parsed_mask_xml():
    base_dir = os.getcwd()
    xml_path = os.path.join(base_dir, "test", "report_mask.xml")
    tree = ET.parse(xml_path)
    return tree


@pytest.fixture
def config_module_file_version():
    base_dir = os.getcwd()  # Get the current directory where pytest is run
    config_path = os.path.join(base_dir, "configure", "CONFIG_MODULE")
    with open(config_path, "r") as file:
        content = file.read()

    match = re.search(r"E3_MODULE_VERSION\s*:=\s*(.+)", content)
    if match:
        return match.group(1).strip()
    return None


def test_compare_versions(parsed_xml, config_module_file_version):
    xml_version = None
    for property_elem in parsed_xml.findall(".//property"):
        if property_elem.get("name") == "e3-fbis_version":
            xml_version = property_elem.get("value")
            break

    assert xml_version == config_module_file_version


def test_check_tests_and_errors(parsed_xml):
    testsuite_elem = parsed_xml.find(".//testsuite")

    num_tests = int(testsuite_elem.get("tests"))
    num_errors = int(testsuite_elem.get("errors"))

    if num_tests == 12130 and num_errors == 0:
        assert True
    else:
        assert False


def test_check_mask_tests_and_errors(parsed_mask_xml):
    testsuite_elem = parsed_mask_xml.find(".//testsuite")

    num_tests = int(testsuite_elem.get("tests"))
    num_errors = int(testsuite_elem.get("errors"))

    if num_tests == 235 and num_errors == 0:
        assert True
    else:
        assert False
