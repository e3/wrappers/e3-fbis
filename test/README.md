# FBIS IOCs Testing and Emulation

## Overview

This  outlines the process for emulating and testing the FBIS and DLN IOCs for the next commissioning phase (SOUP and target). It includes steps for creating emulated PVs, running tests, and verifying that FBIS PVs work as expected.

## Emulating PVs
To check if FBIS PVs are properly linked to DLN PVs and check if they work as expected is impossible to be done as previously done
in lab o directly in site.
To emulate PVs from the 14 DLN IOCs (2 for each of the 7 DLNs), follow these steps:

## Setting Up and Running the IOC Server and Pytest Suite

Follow these steps to set up and run the IOC server that emulates all DLN IOCs and execute the pytest suite.

### 1. Prepare the Environment

Run the setup script to create all necessary files:

```bash
cd test
sh run_all_scripts.sh
```
### 2. Run the server IOC

```bash
cd test/server
source /epics/base-7.0.8/require/5.1.0/bin/activate
iocsh st.cmd
# For testing the masking mismatch monitoring functions
cd mask
iocsh st.cmd
```

### 3. Run FBIS IOC Tests

```bash
source /epics/base-7.0.8.1/require/5.1.1/bin/activate
cd e3-fbis/
iocsh cmds/st.cmd -l cellMods/
```

## Running the Test Suite

Load the virtual environment as explained in software environement setup in the [README](../README.md).

1. **Navigate to the test directory:**
    ```bash
    cd test/tests
    ```

2. **Run the pytest suite:**
    ```bash
    pytest -s -v --retries 3 --retry-delay 2
    # there is not need to reinitialize most of times
    pytest -s -v --retries 3 --retry-delay 2 -m "not one"
    # and creating an xml report
    pytest -s -v --retries 3 --retry-delay 2 -m "not one and not mask_mism" --junitxml=report.xml
    # test only masking mismatch monitoring
    pytest -s -v --retries 3 --retry-delay 2 -m "mask_mism" --junitxml=report_mask.xml
    ```

3. **Run the pytest suite only for a specific system:**
    It is convenient to test only for a specific system. To do this can use an argument indicating the system: MPSID, RF, BPM, BLM ...
    ```bash
    pytest --retries 3 --retry-delay 2 --system=BLM
    ```

## **Verify Test Execution**
Ensure that all tests have been executed (check the total number of tests) using the latest version of e3-fbis.

```bash
pytest -v -k test_report.py
```

## Additional Notes

- Ensure that all environment variables and dependencies are correctly set up before running the tests.
