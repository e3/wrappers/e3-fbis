#!/bin/bash

mkdir temp_files
mkdir tests/json_files
mkdir server/iocsh/systems_dlns

cd scripts

# Run from_excel_to_txt_dln_pus.py for each DLN
for dln in DLN01 DLN02 DLN03 DLN04 DLN05 DLN06 DLN07; do
    python from_excel_to_txt_dln_pus.py $dln
    echo "Completed $dln for from_excel_to_txt_dln_pus.py"
done

# Run from_txt_to_json.py for each DLN
for dln in DLN01 DLN02 DLN03 DLN04 DLN05 DLN06 DLN07; do
    python from_txt_to_json.py $dln
    echo "Completed $dln for from_txt_to_json.py"
done

# Merge the results
python merge.py
echo "Merge process completed."

# Concatenate systems
python concatenates_systems.py
echo "System concatenation completed."

# Run from_txt_to_iocsh.py for each DLN
for dln in DLN01 DLN02 DLN03 DLN04 DLN05 DLN06 DLN07; do
    python from_txt_to_iocsh.py $dln
    echo "Completed $dln for from_txt_to_iocsh.py"
done

cd ..

echo "Finished"
