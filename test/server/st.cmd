iocshLoad("./iocsh/systems_dlns/DLN01.iocsh")
iocshLoad("./iocsh/systems_dlns/DLN02.iocsh")
iocshLoad("./iocsh/systems_dlns/DLN03.iocsh")
iocshLoad("./iocsh/systems_dlns/DLN04.iocsh")
iocshLoad("./iocsh/systems_dlns/DLN05.iocsh")
iocshLoad("./iocsh/systems_dlns/DLN06.iocsh")
iocshLoad("./iocsh/systems_dlns/DLN07.iocsh")

iocshLoad("./iocsh/connections.iocsh")
iocshLoad("./iocsh/ipmi_status.iocsh")
iocshLoad("./iocsh/scu_status.iocsh")
iocshLoad("./iocsh/cc_redundancy.iocsh")
iocshLoad("./iocsh/feedbacks.iocsh")
iocshLoad("./iocsh/bp_pbx_bs.iocsh")
iocshLoad("./iocsh/bm_bd.iocsh")
iocshLoad("./iocsh/latch_rearm.iocsh")
iocshLoad("./iocsh/gbp_ebi.iocsh")
iocshLoad("./iocsh/dln_sw_interlock.iocsh")

#- just for not see disconnected pvs in the sections
dbLoadRecords("db/bm_interlock_sec.db")

iocInit()
