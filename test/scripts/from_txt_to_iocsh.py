import argparse


def parse_txt_to_iocsh(file_name):
    with open("../temp_files/" + file_name + ".txt", "r") as file:
        lines = file.read().splitlines()

    with open("../server/iocsh/systems_dlns/" + file_name + ".iocsh", "w") as file:
        for line in lines:
            file.write(f'iocshLoad("./iocsh/pu.iocsh", "alias={line}")\n')


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Convert a text file to iocsh load commands."
    )
    parser.add_argument("filename", type=str, help="The input/output file name")

    args = parser.parse_args()

    parse_txt_to_iocsh(args.filename)
