import os
import glob
import json


def load_json_files_from_folder(folder_path):
    json_files = glob.glob(os.path.join(folder_path, "DLN*.json"))
    json_data = []
    for file in json_files:
        with open(file, "r") as f:
            data = json.load(f)
            json_data.append(data)
    return json_data


def merge_json_data(json_data_list):
    """Merge a list of dictionaries into one."""
    merged_data = {}
    for data in json_data_list:
        merged_data.update(data)
    return merged_data


def save_json_to_file(json_data, output_file):
    """Save merged JSON data to a file."""
    with open(output_file, "w") as f:
        json.dump(json_data, f, indent=4)


def main(folder_path, output_file):
    """Main function to load, merge, and save JSON files from a folder."""
    json_data_list = load_json_files_from_folder(folder_path)
    merged_json_data = merge_json_data(json_data_list)
    save_json_to_file(merged_json_data, output_file)


# Example usage
if __name__ == "__main__":
    folder_path = "../temp_files"
    output_file = "../tests/json_files/systems_signals.json"
    main(folder_path, output_file)
