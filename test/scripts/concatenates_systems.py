import json

import json


def initialize_output():
    return {
        "MPSoS::PBI-AptM": {"sys": []},
        "MPSoS::BCM": {"sys": []},
        "MPSoS::BLM": {"sys": []},
        "MPSoS::BPM": {"sys": []},
        "MPSoS::PBI-Img": {"sys": []},
        "MPSoS::PSS": {"sys": []},
        "MPSoS::RFLPS": {"sys": []},
        "MPSoS::VVF": {"sys": []},
    }


def process_key(key, output, key_mapping):
    for prefix, output_key in key_mapping.items():
        if prefix in key:
            base_id = key.split(":")[0] if prefix == "BCM" else key
            if base_id not in output[output_key]["sys"]:
                output[output_key]["sys"].append(base_id)
            break


def transform_json(input_file, output_file):
    # Load the input JSON file
    with open(input_file, "r") as f:
        data = json.load(f)

    # Initialize the output structure
    output = initialize_output()

    # Define the mapping of key substrings to output structure
    key_mapping = {
        "PBI-AptM": "MPSoS::PBI-AptM",
        "BCM": "MPSoS::BCM",
        "BLM": "MPSoS::BLM",
        "BPM": "MPSoS::BPM",
        "PBI-Img": "MPSoS::PBI-Img",
        "PSS": "MPSoS::PSS",
        "RFS-FIM": "MPSoS::RFLPS",
        "VVF": "MPSoS::VVF",
    }

    # Process each key in the input JSON
    for key in data.keys():
        process_key(key, output, key_mapping)

    # Write the output JSON file
    with open(output_file, "w") as f:
        json.dump(output, f, indent=4)


transform_json(
    "../tests/json_files/systems_signals.json",
    "../tests/json_files/concatenated_systems.json",
)
