from utilities_json import concatenated_systems_file
from utilities import set_system_ok_nok, find_key_with_value
from pytest import mark
from utilities import sleep

from epics import PV


def set_and_get_status(syst, sub_syst, B, suffix, state):
    set_system_ok_nok(sub_syst, B, suffix, state)
    sleep()
    sleep(0.5)  # 0.5 scan of the PV that gets bi and rbi PVs
    ok = PV(f"{syst}FBIS_ok").get()
    bi_ok = PV(f"{syst}FBIS_BI_ok").get()
    rbi_ok = PV(f"{syst}FBIS_RBI_ok").get()
    sys_bi_ok = PV(f"FBIS::BI_ok").get()
    sys_rbi_ok = PV(f"FBIS::RBI_ok").get()
    return ok, bi_ok, rbi_ok, sys_bi_ok, sys_rbi_ok


@mark.order(7)
def test_bso(get_system):
    """
    Tests basically the mpsos OPI PVs for each system.
    So it test if OK, RBI_ok and BI_ok work as expected.
    """

    ass = True

    data = concatenated_systems_file
    syst = find_key_with_value(data, get_system)
    # if it is not a concatenated system
    if syst is None:
        syst = get_system + "::"
    else:
        syst = syst + "-"

    for B in ["A", "B"]:
        ok, bi_ok, rbi_ok, sys_bi_ok, sys_rbi_ok = set_and_get_status(
            syst, get_system, B, "", False
        )
        ok, bi_ok, rbi_ok, sys_bi_ok, sys_rbi_ok = set_and_get_status(
            syst, get_system, B, "lch_n_", False
        )
        # A system is causing a RBI
        if ok == 1 or bi_ok == 0 or rbi_ok == 1 or sys_bi_ok == 0 or sys_rbi_ok == 1:
            ass = ass and False
        ok, bi_ok, rbi_ok, sys_bi_ok, sys_rbi_ok = set_and_get_status(
            syst, get_system, B, "lch_n_", True
        )
        # A system is causing a BI
        if ok == 1 or bi_ok == 1 or rbi_ok == 0 or sys_bi_ok == 1 or sys_rbi_ok == 0:
            ass = ass and False
        ok, bi_ok, rbi_ok, sys_bi_ok, sys_rbi_ok = set_and_get_status(
            syst, get_system, B, "", True
        )
        # A system is not causing anything.
        if ok == 0 or bi_ok == 0 or rbi_ok == 0 or sys_bi_ok == 0 or sys_rbi_ok == 0:
            ass = ass and False

    assert ass
