from pytest import mark

from epics import PV


@mark.order(1)
def test_ioc_version(record_property):
    "Not a real test, but needed to put the version inside the xml report"
    e3_fbis_version = PV("TEST:stefanopavinato-20692:fbisVersion").get()
    record_property("e3-fbis_version", e3_fbis_version)
    assert True
