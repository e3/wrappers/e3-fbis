from utilities_json import systems_signals_file
from utilities import determine_syspv, sleep

import pytest
from pytest import mark
from itertools import product

from epics import PV


@mark.order(3)
def test_input_flt(get_system_pv, get_channels):
    """
    Test the input filtering functionality for a specific PV of the system and channels.
    Checks the behavior of the input and filtering signals under different conditions.
    Asserts the correctness of the filtering process based on the input signals.
    """

    if get_system_pv[0] != "PBI-BCM01" and get_system_pv[0] != "ISrc-LPS:Ctrl-PLC-01":
        pytest.skip("i_flt is meaningful only for BCM01 and IS_PLC")

    BRD = get_channels
    pvs = systems_signals_file[get_system_pv[0]]["pvs"]

    syspv = determine_syspv(get_system_pv[0])

    out = PV(f"{syspv}FBIS_i_flt_{get_channels}")

    for pv in pvs:
        if pv == get_system_pv[1]:
            sig_in = pv + f"_in_ok_{BRD}"
            sig_flt = pv + f"_flt_{BRD}"

            pv_in = PV(sig_in)
            pv_flt = PV(sig_flt)

            ass = True

            for i in [0, 1]:
                for f in [0, 1]:
                    pv_in.put(i)
                    pv_flt.put(f)
                    sleep()
                    in_ok = PV(syspv + "FBIS" + f"_in_ok_{BRD}").get()
                    flt = PV(syspv + "FBIS" + f"_flt_{BRD}").get()
                    i_flt = out.get()
                    if in_ok == 1:
                        if i_flt == 0:
                            ass = ass and False
                    elif flt == 1:
                        if not (in_ok == i_flt):
                            ass = ass and False
                    else:
                        if i_flt == 1:
                            ass = ass and False
    assert ass
