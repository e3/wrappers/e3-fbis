from pytest import mark

from epics import PV

from utilities import set_system_ok_nok, sleep


@mark.order(6)
def test_con(get_conc_system, get_channels):
    """
    Tests if concatenated systems (such as RF, BCM, BLM, and BPM) are properly concatenated by checking
    the high-level PV that indicates whether all subsystems' inputs and outputs are functioning correctly.
    This test is performed for each channel specified.
    """
    if get_conc_system:

        syst = get_conc_system[0]
        subs = get_conc_system[1]
        B = get_channels

        s_o = PV(f"{syst}-FBIS_ok_{B}")
        s_if = PV(f"{syst}-FBIS_i_flt_{B}")

        ass = True
        sleep()

        if s_o.get() == 0 or s_if.get() == 0:
            ass = ass and False

        set_system_ok_nok(subs, B, "in_ok_", False)
        set_system_ok_nok(subs, B, "", False)
        sleep()
        if s_o.get() == 1 or s_if.get() == 1:
            ass = ass and False

        set_system_ok_nok(subs, B, "in_ok_", True)
        set_system_ok_nok(subs, B, "", True)

        assert ass
