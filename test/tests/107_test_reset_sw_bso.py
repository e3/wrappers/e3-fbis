from pytest import mark
from utilities import sleep

from epics import PV


@mark.no_systems
@mark.order(107)
def test_reset_sw_interlock(get_DLN, get_channels):

    if get_channels == "A":
        R = "Ctrl-AMC-03:"
    else:
        R = "Ctrl-AMC-05:"

    ass = True

    fbis_reset_bso = PV("FBIS::clr_sw_bso")

    # Software Set Interlock in DLNs
    bi_pv = PV(f"FBIS-{get_DLN}:{R}#cfg_swoknokbi_set_reg_{get_channels}")
    rbi_pv = PV(f"FBIS-{get_DLN}:{R}#cfg_set_sw_rbi_rg_{get_channels}")
    pbm_bi_pv = PV(f"FBIS-{get_DLN}:{R}#cfg_set_sw_pbm_rg_{get_channels}")
    pbd_bi_pv = PV(f"FBIS-{get_DLN}:{R}#cfg_set_sw_pbd_rg_{get_channels}")

    fbis_reset_bso.put(1)
    sleep(1)
    bi = bi_pv.get()
    rbi = rbi_pv.get()
    pbm_bi = pbm_bi_pv.get()
    pbd_bi = pbd_bi_pv.get()

    if bi != 0 or rbi != 0 or pbm_bi != 0 or pbd_bi != 0:
        ass = ass and False

    sleep(5)  # PV with HIGH field of 5 seconds
    bi = bi_pv.get()
    rbi = rbi_pv.get()
    pbm_bi = pbm_bi_pv.get()
    pbd_bi = pbd_bi_pv.get()

    if bi != 9 or rbi != 9 or pbm_bi != 9 or pbd_bi != 9:
        ass = ass and False
    assert ass
