from pytest import fixture, param
import json


def pytest_addoption(parser):
    parser.addoption(
        "--system", action="store", default=None, help="System name to filter signals"
    )


@fixture(scope="session")
def system_name(request):
    return request.config.getoption("--system")


def load_system_signals(syst=None):
    with open("./json_files/systems_signals.json") as f:
        data = json.load(f)
        pv_list = []
        for system, details in data.items():
            if syst is None:
                for pv in details["pvs"]:
                    pv_list.append((system, pv))
            else:
                if syst in system:
                    for pv in details["pvs"]:
                        pv_list.append((system, pv))
        return pv_list


def load_systems(syst=None):
    with open("./json_files/systems_signals.json") as f:
        data = json.load(f)

        if syst is None:
            return data
        else:
            filtered_data = {key: value for key, value in data.items() if syst in key}
            return filtered_data


def load_conc_systems(syst=None):
    with open("./json_files/concatenated_systems.json") as f:
        data = json.load(f)
        sys_list = []
        for sys, details in data.items():
            if syst is None:
                for s in details["sys"]:
                    sys_list.append((sys, s))
            else:
                if syst in sys:
                    for s in details["sys"]:
                        sys_list.append((sys, s))
        return sys_list


def idfn(val):
    system_name, pv = val
    return f"{system_name}-{pv}"


@fixture(scope="session")
def system_pv_list(system_name):
    return load_system_signals(system_name)


def pytest_generate_tests(metafunc):
    if "get_system_pv" in metafunc.fixturenames:
        system_pv_list = metafunc.config.getoption("--system")
        system_pv_list = load_system_signals(system_pv_list)
        metafunc.parametrize("get_system_pv", system_pv_list, ids=idfn)
    if "get_system" in metafunc.fixturenames:
        system_pv_list = metafunc.config.getoption("--system")
        system_pv_list = load_systems(system_pv_list)
        metafunc.parametrize("get_system", system_pv_list)
    if "get_conc_system" in metafunc.fixturenames:
        system_pv_list = metafunc.config.getoption("--system")
        system_pv_list = load_conc_systems(system_pv_list)
        metafunc.parametrize("get_conc_system", system_pv_list, ids=idsys)


@fixture
def get_system_pv(request):
    system_name, pv = request.param
    return system_name, pv


@fixture
def get_system(request):
    system_name = request.param
    return system_name


@fixture(scope="session", params=["A", "B"])
def get_channels(request):
    ch = request.param
    return ch


@fixture(scope="session", params=["_in_ok", "_mask", "_flt", "_lch_n", ""])
def get_inp(request):
    ins = request.param
    return ins


def idsys(val):
    try:
        system, sys = val
    except:
        system, sys = "None", "None"
    return f"{system}-{sys}"


@fixture
def get_conc_system(request):
    system_name = request.param
    return system_name


@fixture(
    scope="session",
    params=["DLN01", "DLN02", "DLN03", "DLN04", "DLN05", "DLN06", "DLN07"],
)
def get_DLN(request):
    ins = request.param
    return ins
