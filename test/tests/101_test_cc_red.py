from pytest import mark
from utilities import sleep

from epics import PV


@mark.no_systems
@mark.order(101)
def test_bso_cc(get_DLN, get_channels):
    if get_channels == "A":
        R = "Ctrl-AMC-03:"
    else:
        R = "Ctrl-AMC-05:"

    ass = True

    # HW OK
    red_ok = PV("FBIS::redundancy_ok")
    sleep()

    # all inputs intialized at 1
    if red_ok.get() == 0:
        ass = ass and False

    # set one cross check redundancy at 0 and check
    dln_cc = PV(f"FBIS-{get_DLN}:{R}bso_cc_ok_{get_channels}")
    dln_cc.put(0)
    sleep()
    if red_ok.get() == 1:
        ass = ass and False
    dln_cc.put(1)
    sleep()
    if red_ok.get() == 0:
        ass = ass and False

    assert ass
