from utilities_json import systems_signals_file
from epics import PV


def get_first_pvs_entry(data, key):
    if key in data and "pvs" in data[key]:
        return data[key]["pvs"][0]
    else:
        return None


def set_system_ok_nok(syst, B, sig="", v=True):
    pv = get_first_pvs_entry(systems_signals_file, syst)
    pv1 = PV(f"{pv}_{sig}{B}")
    if v:
        pv1.put(1)
    else:
        pv1.put(0)


def find_key_with_value(data, syst):
    for key, value in data.items():
        if isinstance(value, dict):
            for kk, v in value.items():
                if isinstance(v, list) and syst in v:
                    return key
    return None


def determine_syspv(syst):
    # prefixes = ["Apt", "BLM", "BPM", "Img", "RFS", "VVF", "MEBT-010:BMD-Chop-001"]
    # syspv = 'FBIS:' if any(prefix in syst for prefix in prefixes) else 'FBIS::'
    systs_without_disc = [
        "MPSID",
        "MPSMag",
        "MPSVac",
        "MPSTrg",
        "FIST",
        "SIS",
        "LEBT-Chop",
        "PBI-COLL01",
        "AccPSS",
        "TgtPSS",
        "Tgt-TSS1080",
    ]
    if syst in systs_without_disc or "PBI-BCM" in syst:
        syspv = syst + "::"
    else:
        syspv = syst + ":"
    return syspv


import time


def sleep(t=1.1):
    time.sleep(t)
