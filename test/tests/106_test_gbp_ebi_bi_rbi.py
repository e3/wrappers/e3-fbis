from pytest import mark
from utilities import sleep

from epics import PV


@mark.no_systems
@mark.order(106)
def test_gbp(get_DLN, get_channels):
    if get_channels == "A":
        R = "Ctrl-AMC-03:"
    else:
        R = "Ctrl-AMC-05:"

    ass = True

    # HW OK
    bp_ok = PV("FBIS::beam_permit_ok")

    sleep()
    # all inputs intialized at 1
    if bp_ok.get() == 0:
        ass = ass and False

    # set one local beam permit at 0 and check
    bp = PV(f"FBIS-{get_DLN}:{R}opl_global_BP_{get_channels}")
    bp.put(0)
    sleep()
    if bp_ok.get() == 1:
        ass = ass and False
    bp.put(1)
    sleep()
    if bp_ok.get() == 0:
        ass = ass and False

    if not bp_ok.connect(0.1) or not bp.connect(0.1):
        ass = ass and False

    assert ass


@mark.no_systems
@mark.order(106)
def test_ebi(get_DLN, get_channels):
    if get_channels == "A":
        R = "Ctrl-AMC-03:"
    else:
        R = "Ctrl-AMC-05:"

    ass = True

    # HW OK
    ebi_ok = PV("FBIS::EBI_ok")
    sleep()

    # all inputs intialized at 0
    if ebi_ok.get() == 0:
        ass = ass and False

    # set a local ebi to 1 and check
    ebi_x = PV(f"FBIS-{get_DLN}:{R}cfg_local_ebi_{get_channels}")
    ebi_x.put(1)
    sleep()
    if ebi_ok.get() == 1:
        ass = ass and False
    ebi_x.put(0)
    sleep()
    if ebi_ok.get() == 0:
        ass = ass and False

    if not ebi_ok.connect(0.1) or not ebi_x.connect(0.1):
        ass = ass and False

    assert ass


@mark.no_systems
@mark.order(106)
@mark.parametrize("mode", ["OK", "BI", "RBI", "EBI"])
def test_opl_mode(mode):

    ass = True

    opl_mode_pv = PV("FBIS::opl_mode")

    # set all to the same mode
    for i in ["01", "02", "03", "04", "05", "06", "07"]:
        for brd in [("A", "Ctrl-AMC-03"), ("B", "Ctrl-AMC-05")]:
            opl_md = PV(f"FBIS-DLN{i}:{brd[1]}:opl_us_tx_local_st_{brd[0]}")
            opl_md.put(mode)

    sleep()
    opl_mode = opl_mode_pv.get(as_string=True)

    if opl_mode != mode:
        ass = ass and False

    # set one PV to not that value (or a non valid value):
    opl_md = PV(f"FBIS-DLN03:Ctrl-AMC-03:opl_us_tx_local_st_A")
    opl_md.put(7)  # 7 EBI
    sleep()
    opl_mode = opl_mode_pv.get(as_string=True)
    if opl_mode != "EBI":
        ass = ass and False

    # set back to mode value
    opl_md.put(mode)
    sleep()
    opl_mode = opl_mode_pv.get(as_string=True)
    if opl_mode != mode:
        ass = ass and False

    assert ass
