from pytest import mark
from utilities import sleep

from epics import PV


@mark.no_systems
@mark.order(104)
@mark.parametrize(
    "bd",
    [
        "Error",
        "Undefined",
        "None",
        "ISrc",
        "LebtFc",
        "MebtFc",
        "Dtl2Fc",
        "Dtl4Fc",
        "SpkIbs",
        "MblIbs",
        "BeamDump",
        "Target",
    ],
)
def test_beam_dest(bd):

    ass = True

    beam_dest_pv = PV("FBIS::beam_destination")

    # set all to the same BD
    for i in ["01", "02", "03", "04", "05", "06", "07"]:
        for brd in [("A", "Ctrl-AMC-03"), ("B", "Ctrl-AMC-05")]:
            cfg_bd = PV(f"FBIS-DLN{i}:{brd[1]}:cfg_req_PBD_{brd[0]}")
            cfg_bd.put(bd)

    sleep()
    beam_dest = beam_dest_pv.get(as_string=True)

    if beam_dest != bd:
        ass = ass and False

    # set one PV to not that value (or a non valid value):
    cfg_bd = PV(f"FBIS-DLN03:Ctrl-AMC-03:cfg_req_PBD_A")
    cfg_bd.put(15)  # 15 is not a valid value
    sleep()
    beam_dest = beam_dest_pv.get(as_string=True)
    if beam_dest != "Undefined":
        ass = ass and False

    cfg_bd.put(bd)
    sleep()
    beam_dest = beam_dest_pv.get(as_string=True)
    if beam_dest != bd:
        ass = ass and False

    assert ass


@mark.no_systems
@mark.order(104)
@mark.parametrize(
    "bm",
    [
        "Error",
        "Undefined",
        "None",
        "NoBeam",
        "Conditioning",
        "Probe",
        "FastCommissioning",
        "RfTest",
        "StabilityTest",
        "SlowCommissioning",
        "FastTuning",
        "Slowtuning",
        "LongPulseVerification",
        "ShieldingVerification",
        "Production",
    ],
)
def test_beam_mode(bm):
    ass = True

    beam_mode_pv = PV("FBIS::beam_mode")

    # set all to the same BD
    for i in ["01", "02", "03", "04", "05", "06", "07"]:
        for brd in [("A", "Ctrl-AMC-03"), ("B", "Ctrl-AMC-05")]:
            cfg_bm = PV(f"FBIS-DLN{i}:{brd[1]}:cfg_req_PBM_{brd[0]}")
            cfg_bm.put(bm)

    sleep()
    beam_mode = beam_mode_pv.get(as_string=True)

    if beam_mode != bm:
        ass = ass and False

    # set one PV to not that value (or a non valid value):
    cfg_bm = PV(f"FBIS-DLN03:Ctrl-AMC-03:cfg_req_PBM_A")
    cfg_bm.put(15)  # 15 is not a valid value
    sleep()
    beam_mode = beam_mode_pv.get(as_string=True)
    if beam_mode != "Undefined":
        ass = ass and False

    cfg_bm.put(bm)
    sleep()
    beam_mode = beam_mode_pv.get(as_string=True)
    if beam_mode != bm:
        ass = ass and False

    assert ass
