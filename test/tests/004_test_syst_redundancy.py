from utilities_json import systems_signals_file
from utilities import sleep

from pytest import mark

from epics import PV


def set_red_sigs(ss, B, v):
    """
    Helper function to set the state of Process Variables (PVs) in the given set `ss`.
    This function can set all the PVs in the set `ss` (which includes either all the ready PVs or the BP PVs)
    to a specified state: 'ok', 'nok', or a combination where one PV is set to 'ok' and the rest are set to 'nok'.
    The latter scenario is sufficient to induce a mismatch.
    """
    i = 0
    for s in ss:
        if v == "OK":  # all ok
            PV(f"{s}_in_ok_{B}").put(1)
        elif v == "NOK":  # all nok
            PV(f"{s}_in_ok_{B}").put(0)
        else:  # just one ok
            if i == v:
                PV(f"{s}_in_ok_{B}").put(1)
            else:
                PV(f"{s}_in_ok_{B}").put(0)
        i = i + 1


@mark.order(4)
def test_red_ok(get_system, get_channels):
    """
    Tests whether the PV responsible for indicating redundant signals within a
    given system is functioning as expected.


    Parameters:
    - get_system (str): The system for which the signals are retrieved.
    - get_channels (list): List of channels to test the red signals.

    Returns:
    - None"""
    pvs = systems_signals_file[get_system]["pvs"]

    rdy_pvs = [s for s in pvs if "Rdy" in s]
    bp_pvs = [s for s in pvs if "BP" in s]

    # workaround for isplc
    bp0_pvs = [s for s in bp_pvs if "BP00" in s]
    bp1_pvs = [s for s in bp_pvs if "BP01" in s]
    bp2_pvs = [s for s in bp_pvs if "BP02" in s]

    reds = []
    for p in [rdy_pvs, bp_pvs, bp0_pvs, bp1_pvs, bp2_pvs]:
        if len(p) > 1 and len(p) < 5:
            reds.append(p)

    ass = True

    for BRD in get_channels:
        for red in reds:
            red_ok = PV(f"{red[0]}_red_ok_{BRD}")
            for i in range(len(red)):
                set_red_sigs(red, BRD, i)
                sleep()
                if red_ok.get() == 1:
                    ass = ass and False
            set_red_sigs(red, BRD, "NOK")
            sleep()
            if red_ok.get() == 0:
                ass = ass and False
            set_red_sigs(red, BRD, "OK")
            sleep()
            if red_ok.get() == 0:
                ass = ass and False
    assert ass
