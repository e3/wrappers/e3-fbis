from pytest import mark
from utilities import sleep

from epics import PV


@mark.no_systems
@mark.order(103)
@mark.parametrize("md", ["pbd", "pbm"])
def test_cfg(get_DLN, md, get_channels):
    if get_channels == "A":
        R = "Ctrl-AMC-03:"
    else:
        R = "Ctrl-AMC-05:"

    ass = True

    cfg_ok = PV("FBIS::configuration_ok")

    sleep()

    # all inputs intialized at 1
    if cfg_ok.get() == 0:
        ass = ass and False

    # # set one BP(beam permit) at 0 and check
    pbx = PV(f"FBIS-{get_DLN}:{R}cfg_BP_{md}_{get_channels}")
    pbx.put(0)
    sleep()
    if cfg_ok.get() == 1:
        ass = ass and False
    pbx.put(1)
    sleep()
    if cfg_ok.get() == 0:
        ass = ass and False

    assert ass


@mark.no_systems
@mark.order(103)
def test_beam_state(get_DLN):
    ass = True

    bs_ok = PV("FBIS::beam_state_ok")
    sleep()

    # all inputs intialized at 1
    if bs_ok.get() == 0:
        ass = ass and False

    # # set one Beam State not a 20 and check
    # 20 beam state ON
    # 10 beam state OFF
    bs = PV(f"FBIS-{get_DLN}:Ctrl-EVR-01:DbufBInhSwFBIS-Sts")
    bs.put(10)
    sleep()
    if bs_ok.get() == 1:
        ass = ass and False
    bs.put(20)
    sleep()
    if bs_ok.get() == 0:
        ass = ass and False

    assert ass
