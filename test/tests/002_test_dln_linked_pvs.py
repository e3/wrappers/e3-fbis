from utilities_json import systems_signals_file
from utilities import determine_syspv, sleep

from pytest import mark

from epics import PV


@mark.order(2)
def test_input(get_system_pv, get_channels, get_inp):
    """
    Tests whether each signal or Process Variable (PV) associated with a system can
    correctly trigger the system itself. In other words, it verifies that each PV of a
    system is properly linked to the corresponding system PV.

    Parameters:
    - get_system_pv (tuple): A tuple containing the PVs or signals representing the system.
    - get_channels (str): A string specifying the channels involved. Example values include 'A', 'B', etc.
    - get_inp (str): Information about the input, such as 'inp', 'mask', 'flt', etc.

    Returns:
    - None

    Raises:
    - AssertionError: If any PV is not properly linked to the system PV.
    """

    BRD = get_channels
    inp = f"{get_inp}_{BRD}"
    pvs = systems_signals_file[get_system_pv[0]]["pvs"]

    syspv = determine_syspv(get_system_pv[0])

    ass = True

    for pv in pvs:
        if pv == get_system_pv[1]:
            if inp == f"_{BRD}":
                out_ = syspv + "FBIS" + f"_ok_{BRD}"
                out_syst_ = syspv + "FBIS" + "_ok"
            else:
                out_ = syspv + "FBIS" + inp
                out_syst_ = syspv + "FBIS" + f"{get_inp}"

            sig = pv + inp
            pv_inp = PV(sig)

            pv_inp.put(0)
            sleep()
            out = PV(out_)

            if get_inp in ("_mask", "_flt", ""):
                out_syst = PV(out_syst_)

            # check if the PV is running
            if (
                out.get() == 1
                or not out.connect(0.1)
                or (
                    get_inp in ("_mask", "_flt", "")
                    and (out_syst.get() == 1 or not out_syst.connect(0.1))
                )
            ):
                ass = ass and False
            pv_inp.put(1)
            sleep()
            out = PV(out_)
            if out.get() != 1 or (
                get_inp in ("_mask", "_flt", "") and out_syst.get() != 1
            ):
                ass = ass and False

    assert ass


@mark.order(2)
def test_set_mask(get_system_pv):

    pvs = systems_signals_file[get_system_pv[0]]["pvs"]

    syspv = determine_syspv(get_system_pv[0])
    syst_set_msk = PV(f"{syspv}FBIS_sm")

    ass = True

    for BRD in ["A", "B"]:

        for pv in pvs:
            if pv == get_system_pv[1]:
                sig_set_mask = PV(f"{pv}_sm_{BRD}")

        # set Mask to OK
        syst_set_msk.put(1)
        sleep()
        msk = sig_set_mask.get()
        if msk != 2:
            ass = ass and False

        # set "Not Masked"
        syst_set_msk.put(0)
        sleep()
        msk = sig_set_mask.get()
        if msk != 1:
            ass = ass and False

        if not syst_set_msk.connect(0.1) or not sig_set_mask.connect(0.1):
            ass = ass and False

    assert ass
