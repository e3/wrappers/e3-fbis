import json

global str

systems_signals_file = ""
with open("./json_files/systems_signals.json") as f:
    systems_signals_file = json.load(f)

concatenated_systems_file = ""
with open("./json_files/concatenated_systems.json") as f:
    concatenated_systems_file = json.load(f)
