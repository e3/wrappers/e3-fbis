from pytest import mark
from utilities import sleep

from epics import PV


@mark.no_systems
@mark.order(102)
@mark.parametrize(
    "Fb",
    [
        "ISrc-010:PBI-BCM-001:FBIS-BA-Fb",
        "ISrc-010:PBI-BCM-001:FBIS-BA-DL-Fb",
        "LEBT-010:PBI-BCM-001:FBIS-BA-Fb",
        "MEBT-010:PBI-BCM-001:FBIS-BA-Fb",
        "ISrc-010:PBI-BCM-001:FBIS-R-BA-Fb",
        "ISrc-010:PBI-BCM-001:FBIS-R-BA-DL-Fb",
        "LEBT-010:PBI-BCM-001:FBIS-R-BA-Fb",
        "MEBT-010:PBI-BCM-001:FBIS-R-BA-Fb",
        "ISrc-CS:ISS-Magtr-01:FBIS-PS-Fb",
        "ISrc-CS:ISS-Magtr-01:FBIS-RFProbe-Fb",
    ],
)
@mark.parametrize("in_out", ["in_ok_", ""])
def test_fb(Fb, in_out, get_channels):

    ass = True

    if in_out == "in_ok_":
        fb_ok = PV("FBIS::feedbacks_in_ok")
    else:
        fb_ok = PV("FBIS::feedbacks_ok")

    sleep()

    # all inputs intialized at 1
    if fb_ok.get() == 0:
        ass = ass and False

    # # set one cross check redundancy at 0 and check
    fb = PV(f"{Fb}_{in_out}{get_channels}")
    fb.put(0)
    sleep()
    if fb_ok.get() == 1:
        ass = ass and False
    fb.put(1)
    sleep()
    if fb_ok.get() == 0:
        ass = ass and False

    assert ass
