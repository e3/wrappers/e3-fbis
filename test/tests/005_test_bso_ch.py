from utilities_json import systems_signals_file
from utilities import determine_syspv
from utilities import sleep

from pytest import mark
from itertools import product

from epics import PV


@mark.order(5)
def test_bso(get_system_pv, get_channels):
    """
    Tests whether the PVs `BI_ok` and `RBI_ok` function as expected for a given system.
    The function performs this test by interacting with the `lch_n` (non-latched PV) and `dlv_ok` PV for
    each PV associated with the system.
    """
    pvs = systems_signals_file[get_system_pv[0]]["pvs"]

    BRD = get_channels

    syspv = determine_syspv(get_system_pv[0])

    bi_ = f"{syspv}FBIS_BI_ok_{get_channels}"
    rbi_ = f"{syspv}FBIS_RBI_ok_{get_channels}"

    for pv in pvs:
        if pv == get_system_pv[1]:

            pv_in = PV(pv + f"_{BRD}")
            pv_l_n = PV(pv + f"_lch_n_{BRD}")

            ass = True

            for i in [0, 1]:
                for l in [0, 1]:
                    pv_in.put(i)
                    pv_l_n.put(l)
                    sleep()
                    sleep(0.5)  # 0.5 scan of the PV that gets bi and rbi PVs
                    bi = PV(bi_).get()
                    rbi = PV(rbi_).get()
                    if i:
                        if not (bi == 1 and rbi == 1):
                            ass = ass and False
                    else:
                        if l:
                            if not (bi == 0 and rbi == 1):
                                ass = ass and False
                        else:
                            if not (bi == 1 and rbi == 0):
                                ass = ass and False
    assert ass
