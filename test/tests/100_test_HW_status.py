from pytest import mark
from utilities import sleep

from epics import PV


@mark.no_systems
@mark.order(100)
def test_dln_connections(get_DLN, get_channels):
    if get_channels == "A":
        R = "Ctrl-AMC-03:"
    else:
        R = "Ctrl-AMC-05:"

    ass = True

    # HW OK
    hw_ok = PV("FBIS::HW_ok")

    dln_conn = PV(f"FBIS-{get_DLN}::connections_ok")

    # OPL ok
    opl_conn = PV(f"FBIS::opl_connections_ok")

    # Slink ok
    slink_conn = PV("FBIS::slink_connections_ok")

    sleep()
    # all inputs intialized at 1
    if dln_conn.get() == 0 or hw_ok.get() == 0 or opl_conn.get() == 0:
        ass = ass and False

    # set one OPL ds (downstream) at 0 and check
    opl = PV(f"FBIS-{get_DLN}:{R}opl_ds_tx_ln_up_{get_channels}")
    opl.put(0)
    sleep()
    if dln_conn.get() == 1 or hw_ok.get() == 1 or opl_conn.get() == 1:
        ass = ass and False
    opl.put(1)
    sleep()
    if dln_conn.get() == 0 or hw_ok.get() == 0 or opl_conn.get() == 0:
        ass = ass and False

    lane_down = PV(f"FBIS-{get_DLN}:{R}opl_ds_lane_down_{get_channels}")
    lane_down.put(12)
    sleep()
    if dln_conn.get() == 1 or hw_ok.get() == 1 or opl_conn.get() == 1:
        ass = ass and False
    lane_down.put(0)
    sleep()
    if dln_conn.get() == 0 or hw_ok.get() == 0 or opl_conn.get() == 0:
        ass = ass and False

    chan_down = PV(f"FBIS-{get_DLN}:{R}opl_ds_chan_down_{get_channels}")
    chan_down.put(12)
    sleep()
    if dln_conn.get() == 1 or hw_ok.get() == 1 or opl_conn.get() == 1:
        ass = ass and False
    chan_down.put(0)
    sleep()
    if dln_conn.get() == 0 or hw_ok.get() == 0 or opl_conn.get() == 0:
        ass = ass and False

    # set one OPL us (upstream) at 0 and check
    if get_DLN != "DLN07":
        opl = PV(f"FBIS-{get_DLN}:{R}opl_us_tx_ch_up_{get_channels}")
        opl.put(0)
        sleep()
        if dln_conn.get() == 1 or hw_ok.get() == 1 or opl_conn.get() == 1:
            ass = ass and False
        opl.put(1)
        sleep()
        if dln_conn.get() == 0 or hw_ok.get() == 0 or opl_conn.get() == 0:
            ass = ass and False

        lane_down = PV(f"FBIS-{get_DLN}:{R}opl_us_lane_down_{get_channels}")
        lane_down.put(12)
        sleep()
        if dln_conn.get() == 1 or hw_ok.get() == 1 or opl_conn.get() == 1:
            ass = ass and False
        lane_down.put(0)
        sleep()
        if dln_conn.get() == 0 or hw_ok.get() == 0 or opl_conn.get() == 0:
            ass = ass and False

        chan_down = PV(f"FBIS-{get_DLN}:{R}opl_us_chan_down_{get_channels}")
        chan_down.put(12)
        sleep()
        if dln_conn.get() == 1 or hw_ok.get() == 1 or opl_conn.get() == 1:
            ass = ass and False
        chan_down.put(0)
        sleep()
        if dln_conn.get() == 0 or hw_ok.get() == 0 or opl_conn.get() == 0:
            ass = ass and False

    # set one Slink at 0 and check
    slink = PV(f"FBIS-{get_DLN}:{R}slink_1_up_{get_channels}")
    slink.put(0)
    sleep()
    if dln_conn.get() == 1 or hw_ok.get() == 1 or slink_conn.get() == 1:
        ass = ass and False
    slink.put(1)
    sleep()
    if dln_conn.get() == 0 or hw_ok.get() == 0 or slink_conn.get() == 0:
        ass = ass and False

    assert ass


@mark.no_systems
@mark.order(100)
def test_dln_stat(get_DLN):
    ass = True

    # HW OK
    hw_ok = PV("FBIS::HW_ok")

    # all inputs intialized at 1
    dln_stat = PV(f"FBIS-{get_DLN}::status_ok")
    sleep()
    if dln_stat.get() == 0 or hw_ok.get() == 0:
        ass = ass and False

    # set something not a 4 and check
    cpu_stat = PV(f"FBIS-{get_DLN}:Ctrl-CPU-11:State")
    cpu_stat.put(3)
    sleep()
    if dln_stat.get() == 1 or hw_ok.get() == 1:
        ass = ass and False
    cpu_stat.put(4)
    sleep()
    if dln_stat.get() == 0 or hw_ok.get() == 0:
        ass = ass and False

    assert ass


@mark.no_systems
@mark.order(100)
@mark.parametrize(
    "get_SCU",
    [
        "SCU01",
        "SCU02",
        "SCU03",
        "SCU04",
        "SCU05",
        "SCU06",
        "SCU07",
        "SCU08",
        "SCU09",
        "SCU10",
        "SCU11",
        "SCU12",
        "SCU13",
        "SCU14",
        "SCU16",
        "SCU18",
        "SCU20",
        "SCU22",
        "SCU23",
        "SCU24",
        "SCU25",
    ],
)
def test_scu_stat(get_SCU, get_channels):
    ass = True

    # HW OK
    hw_ok = PV("FBIS::HW_ok")

    # all inputs intialized at 1
    scu_stat = PV(f"FBIS-{get_SCU}::status_ok")
    if scu_stat.get() == 0 or hw_ok.get() == 0:
        ass = ass and False

    if get_channels == "A":
        R = "Ctrl-Ser-01"
    else:
        R = "Ctrl-Ser-02"
    # set something not ok and check
    mc_stat = PV(f"FBIS-{get_SCU}:{R}:MC04_stat_{get_channels}")
    mc_stat.put(0)
    sleep()
    if scu_stat.get() == 1 or hw_ok.get() == 1:
        ass = ass and False
    mc_stat.put(1)
    sleep()
    if scu_stat.get() == 0 or hw_ok.get() == 0:
        ass = ass and False

    assert ass
