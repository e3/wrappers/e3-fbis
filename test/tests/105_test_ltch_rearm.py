from pytest import mark
from utilities import sleep

from epics import PV


@mark.no_systems
@mark.order(105)
def test_latch(get_DLN, get_channels):
    if get_channels == "A":
        R = "Ctrl-AMC-03:"
    else:
        R = "Ctrl-AMC-05:"

    ass = True

    # HW OK
    latched = PV("FBIS::latched")
    sleep()

    # all inputs intialized at 0
    if latched.get() == 1:
        ass = ass and False

    # set a local rbi to 1 and check
    rbi_x = PV(f"FBIS-{get_DLN}:{R}cfg_local_rbi_{get_channels}")
    rbi_x.put(1)
    sleep()
    if latched.get() == 0:
        ass = ass and False
    rbi_x.put(0)
    sleep()
    if latched.get() == 1:
        ass = ass and False

    assert ass


@mark.no_systems
@mark.order(105)
def test_rearm(get_DLN, get_channels):

    if get_channels == "A":
        R = "Ctrl-AMC-03:"
    else:
        R = "Ctrl-AMC-05:"

    ass = True

    rearm = PV("FBIS::rearm")

    # rearm PV for eavh single IFC
    dln_rearm = f"FBIS-{get_DLN}:{R}#rearm_{get_channels}"

    # we care only that dln_rearm get processed.
    # So with an helping PV (a counter) we chech that everytime
    # that FBIS::rearm is executed, the counter is incremented by 1
    dln_rearm_tyck = PV(f"FBIS-{get_DLN}:{R}#rearm_{get_channels}_tyck")

    tycks = [0, 0, 0]
    for i in [0, 1, 2]:
        rearm.put(1)
        sleep(1.2)
        tycks[i] = dln_rearm_tyck.get()

    def is_incremented_by_one(arr):
        for i in range(1, len(arr)):
            if arr[i] != arr[i - 1] + 1:
                return False
        return True

    ass = ass and is_incremented_by_one(tycks)

    if not rearm.connect(0.1) or not dln_rearm_tyck.connect(0.1):
        ass = ass and False

    assert ass
