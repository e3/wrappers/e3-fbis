from utilities_json import systems_signals_file

from pytest import mark
from time import sleep

from epics import PV


@mark.order(0)
@mark.one
def test_input_at_1(get_system_pv, get_channels, get_inp):
    """
    Sets all the Process Variables (PVs) associated with DLN IOCs to a value of one.
    Additionally, verifies that the PVs have indeed been set to one.
    This process establishes a known starting state, which helps save time in subsequent tests
    by focusing only on combinations that could trigger a beam switch-off request,
    rather than testing all possible combinations.

    Parameters:
    - get_system_pv (tuple): A tuple containing the system PVs or signals for each system.
    - get_channels (str): Information about the channels involved.
    - get_inp (str): Information about the input source.

    Returns:
    - None
    """

    BRD = get_channels
    inp = f"{get_inp}_{BRD}"
    pvs = systems_signals_file[get_system_pv[0]]["pvs"]
    for pv in pvs:
        if pv == get_system_pv[1]:
            sig = pv + inp
            pv_inp = PV(sig)
            pv_inp.put(1)

            # chech also that the PV is running
            if pv_inp.get() == 0 or not pv_inp.connect(0.1):
                assert False
