from utilities_json import systems_signals_file
from utilities import determine_syspv
from utilities import sleep

from pytest import mark
from epics import PV


@mark.order(200)
@mark.mask_mism
def test_mask_mismatch(get_system):

    syspv = determine_syspv(get_system)

    fbis_masked_ok = PV("FBIS::is_masked_ok")
    sys_masked_ok = PV(f"{syspv}FBIS_mask_mismatch")

    expected_msk = PV(f"{syspv}#FBIS_to_be_masked")
    current_msk = PV(f"{syspv}FBIS_mask")

    ass = True

    # Expected and current msk at the beginning are both at zero.
    # --> fbis_masked_ok must be one
    if fbis_masked_ok.get() == 0 or sys_masked_ok.get() == 0:
        ass = ass and False

    # set current mask to Undefined and check
    current_msk.put(2)
    sleep()

    if fbis_masked_ok.get() == 1 or sys_masked_ok.get() == 1:
        ass = ass and False

    # set current mask to Masked
    current_msk.put(1)
    sleep()

    if fbis_masked_ok.get() == 1 or sys_masked_ok.get() == 1:
        ass = ass and False

    # set expected mask to Masked
    expected_msk.put(1)
    sleep()

    if fbis_masked_ok.get() == 0 or sys_masked_ok.get() == 0:
        ass = ass and False

    # Reput at 0
    expected_msk.put(0)
    current_msk.put(0)
    sleep()
    if fbis_masked_ok.get() == 0 or sys_masked_ok.get() == 0:
        ass = ass and False

    assert ass
