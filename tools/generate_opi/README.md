# What does the code do
The [make_csv.py](./make_csv.py) creates a union of the DLN0X parametrization files and makes a csv out of the union. Additional columns "Dln_Name,Device_Type,System_Name1" are addded to csv. Some exepction handling in the code can be found.

The [make_opi.py](./make_opi.py) uses functions defined in opi_xml.py and creates .bob files for the opi. A user can define how to group different devices e.g. d_types_2 = ["BCM","BLM","BPM"] found in [make_opi.py](./make_opi.py).

# Running the code
There are two files to run in the following order
1. make_csv.py
2. make_opi.py

bob files will be generated in the [bob](./bob/) directory.
