def make_starter(height: float):
    xml = """<?xml version="1.0" encoding="UTF-8"?>
<display version="2.0.0">
  <name>Accelerator Systems details</name>
  <width>790</width>
  <height>800</height>
  <widget type="rectangle" version="2.0.0">
    <name>Rectangle_2</name>
    <width>790</width>
    <height>{height}</height>
    <line_width>0</line_width>
    <line_color>
      <color name="GROUP-BORDER" red="150" green="155" blue="151">
      </color>
    </line_color>
    <background_color>
      <color name="GROUP-BACKGROUND" red="200" green="205" blue="201">
      </color>
    </background_color>
    <corner_width>10</corner_width>
    <corner_height>10</corner_height>
  </widget>\n
""".format(
        height=height
    )
    return xml


def make_brd():
    xml = """<widget type="label" version="2.0.0">
    <name>Label_12</name>
    <text>$(BRD)</text>
    <width>790</width>
    <height>75</height>
    <font>
      <font family="Source Sans Pro" style="BOLD" size="26.0">
      </font>
    </font>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>\n"""
    return xml


def make_column_headers(
    header_txt: str,
    x_coordinate: float,
    y_coordinate: float,
    height: float,
    width: float,
    label: str,
):
    xml = """<widget type="label" version="2.0.0">
    <name>{label}</name>
    <text>{header_txt}</text>
    <x>{x_coordinate}</x>
    <y>{y_coordinate}</y>
    <width>{width}</width>
    <height>{height}</height>
    <font>
      <font family="Source Sans Pro Semibold" style="REGULAR" size="18.0">
      </font>
    </font>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>\n""".format(
        header_txt=header_txt,
        x_coordinate=x_coordinate,
        y_coordinate=y_coordinate,
        label=label,
        height=height,
        width=width,
    )
    return xml


def make_ender():
    xml = """</display>"""
    return xml


def make_widget(
    system_name: str,
    fbis: str,
    upper_dln: str,
    lower_dln: str,
    index: int,
    y_coordinate: float,
):
    y_coordinate_detail_button = y_coordinate + 9
    xml = """  <widget type="embedded" version="2.0.0">
    <name>Embedded Display_{index}</name>
    <macros>
      <SYSTEM>{system_name}</SYSTEM>
      <FBIS>{fbis}</FBIS>
    </macros>
    <file>../../99-Shared/systems/input_systems/shared/input_system_detailed.bob</file>
    <x>8</x>
    <y>{y_coordinate}</y>
    <width>770</width>
    <height>45</height>
    <resize>2</resize>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Action Button_2</name>
    <actions>
      <action type="open_display">
        <file>../../99-Shared/dln/DLN_instances/{upper_dln}/{lower_dln}_overview.bob</file>
        <target>tab</target>
        <description>Open Display</description>
      </action>
    </actions>
    <text>Details</text>
    <x>703</x>
    <y>{y_coordinate_detail_button}</y>
    <width>60</width>
    <height>27</height>
    <font>
      <font family="Source Sans Pro Semibold" style="REGULAR" size="16.0">
      </font>
    </font>
    <tooltip>$(actions)</tooltip>
  </widget>\n""".format(
        system_name=system_name,
        fbis=fbis,
        upper_dln=upper_dln,
        lower_dln=lower_dln,
        index=index,
        y_coordinate=y_coordinate,
        y_coordinate_detail_button=y_coordinate_detail_button,
    )
    return xml


def make_scrolling_list(system_name: str) -> str:
    xml = """<?xml version="1.0" encoding="UTF-8"?>
<display version="2.0.0">
  <name>{system_name}_scrolling</name>
  <height>760</height>
  <background_color>
    <color red="220" green="225" blue="221" alpha="0">
    </color>
  </background_color>
  <widget type="navtabs" version="2.0.0">
    <name>Navigation Tabs</name>
    <tabs>
      <tab>
        <name>Tab 1</name>
        <file>{system_name}.bob</file>
        <macros>
        </macros>
        <group_name></group_name>
      </tab>
    </tabs>
    <x>0</x>
    <y>0</y>
    <width>790</width>
    <height>700</height>
    <tab_width>0</tab_width>
    <tab_height>0</tab_height>
    <selected_color>
      <color name="Background" red="255" green="255" blue="255">
      </color>
    </selected_color>
    <deselected_color>
      <color name="Background" red="255" green="255" blue="255">
      </color>
    </deselected_color>
  </widget>
</display>""".format(
        system_name=system_name
    )
    return xml
