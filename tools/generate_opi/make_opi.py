from typing import List
from opi_xml import (
    make_brd,
    make_starter,
    make_column_headers,
    make_ender,
    make_widget,
    make_scrolling_list,
)
import re
import os
import pandas as pd


def accelerator_system_details(
    df: pd.DataFrame,
    header_titles: List[str],
    x_coordinates: List[float],
    y_coordinates: List[float],
    heights: List[float],
    widths: List[float],
    labels: List[str],
    file_name: str,
):
    total_height = 200
    for index, rows in df.iterrows():
        total_height += 52
    # device_type = df.loc[0,"Device_Type"]
    file_path = "bob/"
    if not os.path.exists(file_path):
        os.makedirs(file_path)
    with open(file_name, "w") as file:
        file.write(make_starter(total_height))
        file.write(make_brd())
        for i in range(len(header_titles)):
            file.write(
                make_column_headers(
                    header_txt=header_titles[i],
                    x_coordinate=x_coordinates[i],
                    y_coordinate=y_coordinates[i],
                    height=heights[i],
                    width=widths[i],
                    label=labels[i],
                )
            )
        y_coordinate = 175
        # initial_dln = df.loc[0,'Dln_Name']
        for index, rows in df.iterrows():
            system_name = rows["System_Name1"]
            dln = rows["Dln_Name"]
            # if dln == initial_dln:
            if ":" in system_name:
                fbis = "FBIS:"
                file.write(
                    make_widget(
                        system_name, fbis, dln.upper(), dln.lower(), index, y_coordinate
                    )
                )
            else:
                fbis = "FBIS::"
                file.write(
                    make_widget(
                        system_name, fbis, dln.upper(), dln.lower(), index, y_coordinate
                    )
                )

            y_coordinate += 50
            # else:
            #   y_coordinate += 50
            #   file.write(make_widget(system_name,dln.upper(),dln.lower(),index,y_coordinate))
            #   y_coordinate += 50
            #   initial_dln = dln
        file.write(make_ender())
    pattern = r"^bob/([^/]+)\.bob"
    name = re.match(pattern, file_name).group(1)
    with open("bob/" + name + "_scrolling.bob", "w") as file:
        file.write(make_scrolling_list(name))


def make_group(df: pd.DataFrame, d_types: list, save_file_name) -> None:
    group = df[df["Device_Type"].str.contains("|".join(d_types))]
    group = group[["System_Name1", "Dln_Name"]]
    group_unique = group.drop_duplicates()
    group_file_name = save_file_name
    accelerator_system_details(
        group_unique,
        header_titles,
        header_x_coordinates,
        header_y_coordinates,
        header_heights,
        header_widths,
        header_labels,
        group_file_name,
    )
    return None


if __name__ == "__main__":
    header_titles = ["Status", "Input System", "Mask", "Filtered", "Red Check", "Input"]
    header_x_coordinates = [9, 84, 283, 431, 525, 623]
    header_y_coordinates = [120, 120, 120, 120, 100, 120]
    header_heights = [30, 30, 30, 30, 60, 30]
    header_widths = [60, 120, 76, 70, 70, 60]
    header_labels = [
        "Label_11",
        "Label_8",
        "Label_9",
        "Label_10",
        "Label_14",
        "Label_7",
    ]

    csv_dir = "csv"
    union_file_name = "union.csv"
    df = pd.read_csv(os.path.join(csv_dir, union_file_name))

    d_types_1 = ["RF", "IS_PLC", "LEBT", "MEBT"]
    group1_file_name = "bob/group1.bob"
    make_group(df, d_types_1, group1_file_name)

    d_types_2 = ["BCM", "BLM", "BPM"]
    group2_file_name = "bob/group2.bob"
    make_group(df, d_types_2, group2_file_name)

    d_types_3 = ["MPSMAG", "MPSVAC", "MPSID", "MPSTrg"]
    group3_file_name = "bob/group3.bob"
    make_group(df, d_types_3, group3_file_name)
