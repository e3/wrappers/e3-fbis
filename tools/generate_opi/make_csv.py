import os
import pandas as pd
import numpy as np


def device_types(d_types: list, func, base_data):
    """
    This function recursively calls func. For example
    if d_types = ["type1","type2","type3"] then device_types
    returns func("type1",func("type2",func("type3",base_data)))
    """
    if len(d_types) == 1:
        return func(d_types[0], base_data)
    else:
        return func(d_types[0], device_types(d_types[1:], func, base_data))


if __name__ == "__main__":
    para_folder = "../../param_files/Parameterization_Files_Excel"
    para_files = os.listdir(os.path.join(para_folder))
    para_files.remove(".gitkeep")
    para_files.remove("DLN00.xlsm")
    para_files.remove("README")
    para_files.sort()

    dln_pu_sheet = "DLN_PU"
    args_dln_pu = {
        "io": None,
        "sheet_name": dln_pu_sheet,
        "skiprows": 5,
        "usecols": "A,B,C,D,E,K:N",
        "engine": "openpyxl",
    }

    def new_col_names(df: pd.DataFrame) -> list:
        cols = df.columns
        new_cols = []
        for col in cols:
            new_cols.append(col.replace("\n", "_").replace(" ", "_"))
        return new_cols

    df_li = []
    for file_name in para_files:
        file_full_name = os.path.join(para_folder, file_name)
        dln = file_name.split(".")[0]
        args_dln_pu["io"] = file_full_name
        print(f"Opening {file_name}")
        df = pd.read_excel(**args_dln_pu)
        df.columns = new_col_names(df)
        df["Dln_Name"] = dln
        df_li.append(df)
    print("Creating union")
    df_union = pd.concat(df_li)

    # Made a new function so I do not need to write np.where and search condition
    filter_device_types = lambda d_type, data: np.where(
        df_union["Prefix"].str.contains(d_type), d_type, data
    )

    d_types = ["RF", "BCM", "BPM", "BLM", "Apt", "Grd", "Img", "VVF"]
    df_union["Device_Type"] = device_types(
        d_types, filter_device_types, df_union["Prefix"]
    )

    # Created two columns because regex or statements creates two columns
    df_union["System_Name1"] = df_union["Alias"].str.extract(
        r"(^[^:]+:[^:]+)|(^[^:]+)"
    )[0]
    df_union["System_Name2"] = df_union["Alias"].str.extract(
        r"(^[^:]+:[^:]+)|(^[^:]+)"
    )[1]

    # Combining the two columns into one
    df_union["System_Name1"].update(df_union.pop("System_Name2"))

    # Handling exceptions in System_Name1 field
    df_union.loc[
        df_union["System_Name1"].str.contains("MEBT-010:BMD-Chop-001"), "System_Name1"
    ] = "MEBT-Chop"
    df_union.loc[
        df_union["System_Name1"].str.contains("ISrc-LPS:Ctrl-PLC-01"), "System_Name1"
    ] = "IS_PLC"
    df_union.loc[
        df_union["System_Name1"].str.contains("ISrc-LPS:Ctrl-PLC-01"), "System_Name1"
    ] = "IS_PLC"

    ##Removing because feedbacks
    field = "System_Name1"
    condition = (
        (df_union[field].str.contains("ISrc-010:PBI-BCM-001"))
        | (df_union[field].str.contains("LEBT-010:PBI-BCM-001"))
        | (df_union[field].str.contains("MEBT-010:PBI-BCM-001"))
    )
    df_union = df_union.drop(df_union[condition].index)

    csv_dir = "csv"
    if not os.path.exists(csv_dir):
        os.mkdir(csv_dir)
    union_file_name = "union.csv"
    df_union.to_csv(os.path.join(csv_dir, union_file_name), index=False)
