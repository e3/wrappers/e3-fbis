import pandas as pd


def epics_env_set_gen(macro: str, value: str, max_length: int) -> str:
    """
    This functions returns string that look like
    epicsEnvSet("RFQ-010:RFS-FIM-101",       "RFQ-010:RFS-FIM-101")
    or
    epicsEnvSet("SYS",         "RF").
    The max_length variable is related to the longest name a device group has.
    So if RFQ-010:RFS-FIM-101 is longest name in the RF group then
    max_length = len("RFQ-010:RFS-FIM-101")
    """
    if macro == value:
        spacing = "".join([" " for _ in range(4 + max_length - len(value))])
        return f"""epicsEnvSet("{macro}",""" + spacing + f""""{value}")\n"""
    else:
        return f"""epicsEnvSet("{macro}"     "{value}")\n"""


def iocsh_load_systems(sys_iocsh: str, system_name: str) -> str:
    return f"""iocshLoad("$(fbis_DIR){sys_iocsh}",    "SYS=$({system_name})")\n"""


def iocsh_load_concat(
    system_iocsh: str, group_name: str, df_subgroup: pd.DataFrame
) -> str:
    devices = df_subgroup["System_Name"].values.tolist()
    args = ", ".join([f"S{i}=$({device})" for i, device in enumerate(devices)])
    return f"""iocshLoad("$(fbis_DIR){system_iocsh}","SYS=$(SYS), {args}")"""


def db_load_recs_chk_chan(group_name: str) -> str:
    return f"""dbLoadRecords("input_system_check_chans.db", "SYS=$(SYS)")"""


def make_iocsh_file(df_subgroup: pd.DataFrame) -> str:

    # Check if data frame has expected format
    ## Checking if expected devices types are in dataframe
    expected_device_types = ["RF", "BLM", "BPM"]
    unique_device_types = df_subgroup["Device_Type"].unique()
    for d_type in unique_device_types:
        if d_type == "nBLM" or d_type == "ICBLM":
            pass
        else:
            check = any([d_type in exp_type for exp_type in expected_device_types])
            msg = f"d_type {d_type} does not exist in expected_device_types {expected_device_types}"
            assert check, msg

    ## Checking if how many unique device types are there
    """
        We expect only one RF and BPM. We expect only two
        BLMs: icBLMs and nBLMs.
    """
    condition1 = len(df_subgroup["Device_Type"].unique()) == 1
    condition2 = len(df_subgroup["Device_Type"].unique()) == 2
    if condition1:
        pass
    elif condition2:
        device_types = df_subgroup["Device_Type"].unique()
        msg = f"only BLM should have two types not {device_type}"
        assert all(["BLM" in d_type for d_type in device_types]), msg
    else:
        msg = "There are more than one device types in the data frame. Please make sure only one type exist"
        assert False, msg

    # defining variables
    group_name = df_subgroup["Device_Type"].iloc[0]
    if "BLM" in group_name:
        group_name = "BLM"
    sys_iocsh = f"{group_name.lower()}_sys.iocsh"
    system_iocsh = f"{group_name.lower()}_systems.iocsh"
    max_length = df_subgroup["System_Name"].str.len().max()

    file_str = ""

    # Set environment
    ## Set $Sys
    file_str += epics_env_set_gen("SYS", group_name, None)
    ## Set device names
    for i, rows in df_subgroup.iterrows():
        macro = rows["System_Name"]
        value = macro
        file_str += epics_env_set_gen(macro, value, max_length)
    file_str += "\n\n"

    # iocshLoads
    ## iocshLoad Devices
    for i, rows in df_subgroup.iterrows():
        system_name = rows["System_Name"]
        file_str += iocsh_load_systems(sys_iocsh, system_name)
    file_str += "\n\n"

    ## iocshLoad concat
    file_str += f"#Concat different {group_name} systems\n"
    file_str += iocsh_load_concat(system_iocsh, group_name, df_subgroup)
    file_str += "\n\n"

    # dbLoadRecords
    ## loading input_system_check_chans.db

    file_str += f"#-Channel A & B\n"
    file_str += db_load_recs_chk_chan(group_name)
    file_str += "\n\n"
    return file_str


if __name__ == "__main__":
    from configuration import param_union_path

    df = pd.read_csv(param_union_path)

    device_types = ["RF", "BPM", "BLM"]
    iocsh_save_path = "{device_type}.iocsh"
    for device_type in device_types:
        df_subgroup = df[df["Device_Type"].str.contains(device_type)]
        df_subgroup = df_subgroup.drop_duplicates(subset=["System_Name"])
        file_str = make_iocsh_file(df_subgroup)

        # Handeling exceptions
        if device_type == "BPM":
            file_str = file_str.replace(
                '''"$(fbis_DIR)bpm_sys.iocsh",    "SYS=$(PBI-BPM13:Ctrl-AMC-120)"''',
                """$(fbis_DIR)bpm_sys_en.iocsh",    "SYS=$(PBI-BPM13:Ctrl-AMC-120)""",
            )
            file_str = file_str.replace(
                '''"$(fbis_DIR)bpm_sys.iocsh",    "SYS=$(PBI-BPM13:Ctrl-AMC-130)"''',
                """$(fbis_DIR)bpm_sys_en.iocsh",    "SYS=$(PBI-BPM13:Ctrl-AMC-130)""",
            )

        with open(iocsh_save_path.format(device_type=device_type.upper()), "w") as file:
            file.write(file_str)
