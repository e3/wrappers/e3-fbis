# What does the code do?
There three scripts in this directory
1. [generate_iocsh.py](./generate_iocsh.py)
2. [generate_modules.py](./generate_modules.py)
3. [generate_set_msk.py](./generate_set_msk.py)

## generate_iocsh.py
This script generates for example the [RF.iocsh](./../../iocsh/RF.iocsh) file.

## generate_modules.py
This script generates the [modules_91_msk.template](./../../fbis/Db/modules_91_concatenate_chan.template) and [rf_rfs_concat.substituitions](./../../fbis/Db/rf_rf_concat.substitutions) file.

## generate_set_msk.py
This script generate files {device_type}_set_msk.template. This code is generates files that are no longer needed.

# Running the code
Make sure all the submodules listed in [.gitmodules](./../../.gitmodules) have been initialized. This code uses the param_file_union.csv file generated in repository https://gitlab.esss.lu.se/ics-protection/fbis/fbis-files-scripts .

To run the script type
```cli
python3 <script name>.py
```
You can edit the save path for each script by opening each script and editing the path.
