from generate_modulus import Node, make_group, make_tree, encode, write_code
import string


def write_dfo_pv(tree: Node):
    alphabets = string.ascii_uppercase
    s = f"""record (dfanout, "FBIS::#$(SYS)_mm_dfo_$(BRD){tree.index}")\n"""
    s += "{\n"
    s += """  field(DESC, "$(SYS) mask fanout")\n"""
    s += """  field(SELM,"ALL")\n"""
    for i, child in enumerate(tree.children):
        s += f"""  field(OUT{alphabets[i]}, "FBIS::#$(SYS)_mm_dfo_$(BRD){child.index}")\n"""
    s += "}\n\n"
    return s


def write_dfo_device(tree: Node):
    alphabets = string.ascii_uppercase
    s = f"""record (dfanout, "FBIS::#$(SYS)_mm_dfo_$(BRD){tree.index}")\n"""
    s += "{\n"
    s += """  field(DESC, "$(SYS) mask fanout")\n"""
    s += """  field(SELM, "All")\n"""

    for i in range(len(tree.children)):
        s += f"""  field(OUT{alphabets[i]}, "FBIS::$(S{tree.children[i]})_sm_$(BRD)")\n"""
    s += "}\n\n"
    return s


def write_fo_pv(tree: Node):
    # mapping = {i:i for i in range(10)}
    s = f"""record (fanout, "FBIS::#$(SYS)_mm_fo_$(BRD){tree.index}")\n"""
    s += "{\n"
    s += """  field(DESC, "$(SYS) mask fanout")\n"""
    s += """  field(SELM, "All")\n"""

    for i, child in enumerate(tree.children):
        s += f"""  field(LNK{i}, "FBIS::#$(SYS)_mm_fo_$(BRD){child.index}")\n"""

    s += "}\n\n"
    return s


def write_fo_device(tree: Node):
    alphabets = string.ascii_uppercase
    s = f"""record (fanout, "FBIS::#$(SYS)_mm_fo_$(BRD){tree.index}")\n"""
    s += "{\n"
    s += """  field(DESC, "$(SYS) mask fanout")\n"""
    s += """  field(SELM, "All")\n"""

    for i in range(len(tree.children)):
        s += f"""  field(LNK{i}, "FBIS::$(S{tree.children[i]})_sm_$(BRD)")\n"""
    s += "}\n\n"
    return s


def make_set_msk(num_devices: int, save_path: str):
    group_size = 8
    xs = [i for i in range(1, num_devices + 1)]

    grouping = lambda xs: make_group(group_size, xs)

    write_dfo = lambda s, tree: write_code(s, write_dfo_device, write_dfo_pv, tree)
    write_fo = lambda s, tree: write_code(s, write_fo_device, write_fo_pv, tree)

    tree = make_tree(xs, grouping)
    en_tree = encode("", tree)

    dfo = write_dfo("", en_tree)
    fo = write_fo("", en_tree)

    first_pv = """
record (bo, "FBIS::$(SYS)_sm_$(BRD)")
{
  field (DESC, "Set Mask Mode")
  field (DTYP, "Soft Channel")
  field (ZNAM, "Not masked")
  field (ONAM, "Masked to OK")
  field (OUT, "FBIS::#$(SYS)_mm_dfo_$(BRD) PP")
  field (FLNK, "FBIS::#$(SYS)_mm_fo_$(BRD) PP")
  field (ASG,"mps-critical")
  info(autosaveFields, "VAL")
}\n
"""
    set_mask = first_pv + dfo + fo

    with open(save_path, "w") as file:
        file.write(set_mask)

    return None


if __name__ == "__main__":
    import os
    import pandas as pd
    from configuration import param_union_path, device_types

    df = pd.read_csv(os.path.join(param_union_path))

    # save_path = "../../fbis/Db/{device_type}_set_msk.template"
    save_path = "{device_type}_set_msk.template"
    for device_type in device_types:
        df_types = df[df["Device_Type"].str.contains(device_type)]
        df_types = df_types.drop_duplicates(subset=["System_Name"])
        num_devices = len(df_types)
        make_set_msk(num_devices, save_path.format(device_type=device_type.upper()))
        print(f"Wrote to {save_path.format(device_type= device_type.upper())}")
