import string
import math
import os
from typing import List, Callable


import string


class Node:
    def __init__(self, index):
        self.index = index
        self.children = []

    def __repr__(self):
        return f"Node<{(self.children,self.index)}>"

    def add_child(self, obj):
        self.append(obj)
        return self

    def add_children(self, objs):
        for obj in objs:
            self.children.append(obj)
        return self


def make_group(group_size, xs):
    if not xs:
        return []
    elif len(xs) <= group_size:
        return [xs]
    else:
        return [[xs[i] for i in range(group_size)]] + make_group(
            group_size, xs[group_size:]
        )


def make_tree(xs, f):
    if len(xs) == 1:
        return xs[0]
    else:
        return make_tree([Node(None).add_children(objs) for objs in f(xs)], f)


def encode(s: str, tree: Node):
    if type(tree.children[0]) != Node:
        tree.index = s
        return tree
    else:
        tree.index = s
        for i, child in enumerate(tree.children):
            new_str = s + f"_{i}"
            encode(new_str, child)
        return tree


def write_pv_function(tree: Node):
    alphabets = string.ascii_uppercase
    if tree.index == "":
        s = f"""record (calcout, "FBIS::#$(SYS)_$(PUFIELD)_$(BRD)")\n"""
        s += "{\n"
        for i, child in enumerate(tree.children):
            s += f"""  field(INP{alphabets[i]}, "FBIS::#$(SYS)_$(PUFIELD)_$(BRD){child.index} CPP")\n"""

        and_stmt = " AND ".join(alphabets[: len(tree.children)])
        s += f"""  field(CALC, "{and_stmt}")\n"""
        s += f"""  field(OUT,"FBIS::$(SYS)_$(PUFIELD)_$(BRD) PP")"""
        s += "}\n\n"
    else:
        s = f"""record (calc, "FBIS::#$(SYS)_$(PUFIELD)_$(BRD){tree.index}")\n"""
        s += "{\n"
        for i, child in enumerate(tree.children):
            s += f"""  field(INP{alphabets[i]}, "FBIS::#$(SYS)_$(PUFIELD)_$(BRD){child.index} CPP")\n"""

        and_stmt = " AND ".join(alphabets[: len(tree.children)])
        s += f"""  field(CALC, "{and_stmt}")\n"""
        s += "}\n\n"

    return s


def write_device_function(tree: Node):
    alphabets = string.ascii_uppercase
    s = f"""record (calc, "FBIS::#$(SYS)_$(PUFIELD)_$(BRD){tree.index}")\n"""
    s += "{\n"
    for i in range(len(tree.children)):
        device_index = tree.children[i]
        s += f"""  field(INP{alphabets[i]}, "FBIS:$(S{device_index})_$(PUFIELD)_$(BRD) CPP")\n"""

    and_stmt = " AND ".join(alphabets[: len(tree.children)])
    s += f"""  field(CALC, "{and_stmt}")\n"""
    s += "}\n\n"
    return s


def write_code(s: str, f1: Callable, f2: Callable, tree: Node):
    if type(tree.children[0]) != Node:
        s += f1(tree)
        return s
    else:
        s += f2(tree)
        for child in tree.children:
            s += write_code("", f1, f2, child)
        return s


def make_modules(num_device: int, save_path: str):
    xs = [i for i in range(1, num_device + 1)]
    group_size = 12

    grouping = lambda xs: make_group(group_size, xs)

    tree = make_tree(xs, grouping)
    s = ""
    en_tree = encode(s, tree)

    write_modulus = lambda s, tree: write_code(
        s, write_device_function, write_pv_function, tree
    )
    modulus = """
record(bi, "FBIS::$(SYS)_$(PUFIELD)_$(BRD)")
{
  field (DESC, "$(SYS) $(PUFIELD)")
  field (ZNAM, "NOK")
  field (ONAM, "OK")
  field (ZSV,  "MAJOR")
  field (OSV,  "NO_ALARM")
  info(aa_policy, "default")
  info(archiver, "tn")
}
"""
    modulus += write_modulus("", en_tree)

    with open(save_path, "w") as file:
        file.write(modulus)
    return None


def make_concat_subs(device_type: str, num_device: int, save_path: str):
    input_parameters = ", ".join([f"S{i}" for i in range(1, num_device + 1)])
    parameters = ", ".join([f"$(S{i})" for i in range(1, num_device + 1)])
    s = """file "modules_{num_device}_concatenate_chan.template" """.format(
        num_device=num_device
    )
    s += """{ pattern\n"""
    s += """{"""
    s += f"""      SYS,      PUFIELD,     BRD,     {input_parameters} """
    s += "}\n\n"
    fields = ["i_flt", "flt", "mask", "ok", "RBI_ok", "BI_ok"]
    for field in fields:
        s += """{{$(SYS),      "{field}",   $(BRD),  {parameters}}}\n""".format(
            field=field, parameters=parameters
        )
    s += "}\n"
    with open(save_path, "w") as file:
        file.write(s)
    return None


if __name__ == "__main__":
    import os
    import pandas as pd
    from configuration import param_union_path, device_types

    df = pd.read_csv(os.path.join(param_union_path))
    for device_type in device_types:
        df_types = df[df["Device_Type"].str.contains(device_type)]
        df_types = df_types.drop_duplicates(subset=["System_Name"])
        num_devices = len(df_types)
        # save_path =f"../../fbis/Db/modules_{num_devices}_concatenate_chan.template"
        save_path = f"modules_{num_devices}_concatenate_chan.template"
        make_modules(num_devices, save_path)
        print(f"Wrote to {save_path}")
        d_type = device_type.lower()
        # save_path_concat = f"../../fbis/Db/{d_type}_{d_type}_concat.substitutions"
        save_path_concat = f"{d_type}_{d_type}_concat.substitutions"
        make_concat_subs(device_type, num_devices, save_path_concat)
        print(f"Wrote to {save_path_concat}")
