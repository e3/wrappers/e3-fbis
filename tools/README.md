# Scripts
In this directory you can find multiple scripts

## generate_pv
To learn how use the generate_pv script read [generate_pv/README.md](./generate_pv/README.md)

## generate_opi
To learn how to use the generate_opi script read [generate_opi/README.md](./generate_opi/README.md)

## generate_opi_concatenated systems
To learn how to use the generate_opi_concatenated_systems script read [generate_opi_concatenated_systems/README.md](./generate_opi_concatenated_systems/README.md)

## mask_systems systems
To learn how to use the mask_systems script read [mask_systems/README.md](./mask_systems/README.md)
