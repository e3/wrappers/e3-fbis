import pandas as pd
import argparse

# Setup argparse to handle command-line arguments
parser = argparse.ArgumentParser(
    description="Extract a column from an Excel sheet and save it to a text file."
)
parser.add_argument("dln_instance", type=str, help="DLN01, DLN02 ...")
args = parser.parse_args()

excel_file = f"../../param_files/Parameterization_Files_Excel/{args.dln_instance}.xlsm"

# Read the specific column from the Excel file
df = pd.read_excel(excel_file, sheet_name="DLN_PU", usecols=[13])

# Convert the column to a list
column_data = df.squeeze().tolist()[5:]

output_file = f"temp_files/{args.dln_instance}.txt"

# Save the column data to a text file
with open(output_file, "w") as f:
    for item in column_data:
        f.write(f"{item}\n")
