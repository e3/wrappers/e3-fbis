import argparse
import json
import os


def parse_txt_to_json(dln_instance):

    input_file = f"temp_files/{dln_instance}.txt"
    output_file = f"temp_files/opi_{dln_instance}.json"

    data = {}

    with open(input_file, "r") as file:
        lines = file.read().splitlines()

    for line in lines:
        if "BCM01" in line or "PBI-BCM-001" in line:
            key = "PBI-BCM01"
        elif line.startswith("ISrc-LPS"):
            key = "ISrc-LPS:Ctrl-PLC-01"
        elif line.startswith("LEBT-CS"):
            key = "LEBT-Chop"
        elif "MEBT-010:BMD-Chop-001" in line:
            key = "MEBT-010:BMD-Chop-001"
        elif "RFC:" "RFC:" in line or "RFS-FIM" in line:
            parts = line.split(":", 2)
            if len(parts) >= 3:
                prefix = f"{parts[0]}:{parts[1]}"
                suffix = parts[2]
                key = f"{prefix}"
        elif any(x in line for x in ["PBI-AptM", "PBI-Img", "PBI-Grd"]):
            parts = line.split(":", 2)
            if len(parts) >= 3:
                prefix = f"{parts[0]}:{parts[1]}"
                key = f"{prefix}"
        elif "Vac-VVF" in line:
            parts = line.split(":", 2)
            if len(parts) >= 3:
                prefix = f"{parts[0]}:{parts[1]}"
                key = f"{prefix}"
        elif (
            line.startswith("PBI-BPM")
            or line.startswith("PBI-ICBLM")
            or line.startswith("PBI-nBLM")
        ):
            parts = line.split(":", 2)
            if len(parts) >= 3:
                prefix = f"{parts[0]}:{parts[1]}"
                key = f"{prefix}"
        else:
            split_line = line.split(":", 1)
            if "ISrc-CS" == split_line[0]:
                pass
            else:
                key = split_line[0]

        if key not in data:
            data[key] = {"dln": dln_instance, "pvs": []}

        if "-Fb" not in line:  # skip feedback
            data[key]["pvs"].append(line)

    with open(output_file, "w") as outfile:
        json.dump(data, outfile, indent=4)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Convert a text file to a specific JSON format."
    )
    parser.add_argument("input_path", type=str, help="The path to the input text file")

    args = parser.parse_args()

    parse_txt_to_json(args.input_path)
