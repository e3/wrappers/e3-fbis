import json

import json


def initialize_output():
    return {
        "MPSoS::PBI-AptM": {
            "sys": [],
            "dln": [],
            "scu": [],
            "mc_type": [],
            "slot": [],
            "port": [],
        },
        "MPSoS::BCM": {
            "sys": [],
            "dln": [],
            "scu": [],
            "mc_type": [],
            "slot": [],
            "port": [],
        },
        "MPSoS::BLM": {
            "sys": [],
            "dln": [],
            "scu": [],
            "mc_type": [],
            "slot": [],
            "port": [],
        },
        "MPSoS::BPM": {
            "sys": [],
            "dln": [],
            "scu": [],
            "mc_type": [],
            "slot": [],
            "port": [],
        },
        "MPSoS::PBI-Img": {
            "sys": [],
            "dln": [],
            "scu": [],
            "mc_type": [],
            "slot": [],
            "port": [],
        },
        "MPSoS::PSS": {
            "sys": [],
            "dln": [],
            "scu": [],
            "mc_type": [],
            "slot": [],
            "port": [],
        },
        "MPSoS::RFLPS": {
            "sys": [],
            "dln": [],
            "scu": [],
            "mc_type": [],
            "slot": [],
            "port": [],
        },
        "MPSoS::VVF": {
            "sys": [],
            "dln": [],
            "scu": [],
            "mc_type": [],
            "slot": [],
            "port": [],
        },
    }


def process_key(key, output, key_mapping, data):
    for prefix, output_key in key_mapping.items():
        if prefix in key:
            base_id = key.split(":")[0] if prefix == "BCM" else key
            if base_id not in output[output_key]["sys"]:
                output[output_key]["sys"].append(base_id)
                output[output_key]["dln"].append(data[base_id]["dln"])
                output[output_key]["scu"].append(data[base_id]["scu"])
                output[output_key]["mc_type"].append(data[base_id]["mc_type"])
                output[output_key]["slot"].append(data[base_id]["slot"])
                output[output_key]["port"].append(data[base_id]["port"])
            break


def transform_json(input_file, output_file):
    # Load the input JSON file
    with open(input_file, "r") as f:
        data = json.load(f)

    # Initialize the output structure
    output = initialize_output()

    # Define the mapping of key substrings to output structure
    key_mapping = {
        "PBI-AptM": "MPSoS::PBI-AptM",
        "BCM": "MPSoS::BCM",
        "BLM": "MPSoS::BLM",
        "BPM": "MPSoS::BPM",
        "PBI-Img": "MPSoS::PBI-Img",
        "PSS": "MPSoS::PSS",
        "RFS-FIM": "MPSoS::RFLPS",
        "VVF": "MPSoS::VVF",
    }

    # Process each key in the input JSON
    for key in data.keys():
        process_key(key, output, key_mapping, data)

    # Write the output JSON file
    with open(output_file, "w") as f:
        json.dump(output, f, indent=4)


transform_json(
    "temp_files/opi_systems_signals.json", "temp_files/opi_concatenated_systems.json"
)
