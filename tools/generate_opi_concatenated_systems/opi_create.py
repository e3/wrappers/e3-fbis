import json
import xml.etree.ElementTree as ET
import argparse


def create_widget(
    parent, sys_val, dln_val, scu_val, mc_type_val, slot_val, port_val, index
):
    """Create a widget element and append it to the parent node."""
    widget = ET.SubElement(parent, "widget", type="embedded", version="2.0.0")
    ET.SubElement(widget, "name").text = f"Embedded Display_{index}"

    macros = ET.SubElement(widget, "macros")
    ET.SubElement(macros, "SYS").text = sys_val
    ET.SubElement(macros, "DLN").text = dln_val
    ET.SubElement(macros, "dln").text = dln_val.lower()
    ET.SubElement(macros, "SCU").text = "FBIS-" + scu_val
    ET.SubElement(macros, "scu").text = scu_val.lower()
    ET.SubElement(macros, "mc_type").text = mc_type_val.upper()
    ET.SubElement(macros, "slot").text = f"{int(slot_val):02d}"
    ET.SubElement(macros, "port").text = port_val

    ET.SubElement(widget, "file").text = "pane.bob"
    if index > 0:
        ET.SubElement(widget, "y").text = str(30 + 35 * (index % 27))
        ET.SubElement(widget, "x").text = str(20 + 420 * int(index / 27))
    else:
        ET.SubElement(widget, "y").text = str(30)
        ET.SubElement(widget, "x").text = str(20)

    ET.SubElement(widget, "width").text = "420"
    ET.SubElement(widget, "height").text = "35"
    ET.SubElement(widget, "resize").text = "2"
    ET.SubElement(widget, "actions")


def create_filter_mask_widget(parent, filter_mask, initial_x_pos, index):
    err_msg = "variable filter_mask does not match expectations"
    assert any(
        [filter_mask == expectation for expectation in ["Masked", "Filtered"]]
    ), err_msg

    widget = ET.SubElement(parent, "widget", type="label", version="2.0.0")
    ET.SubElement(widget, "name").text = filter_mask
    ET.SubElement(widget, "text").text = filter_mask

    quotient = index // 27
    ET.SubElement(widget, "x").text = str(initial_x_pos + quotient * 420)
    if filter_mask == "Masked":
        ET.SubElement(widget, "width").text = str(60)
    else:
        ET.SubElement(widget, "width").text = str(54)
    ET.SubElement(widget, "height").text = str(30)

    font = ET.SubElement(widget, "font")
    ET.SubElement(
        font, "font", family="Source Sans Pro Semibold", style="REGULAR", size="16.0"
    )

    ET.SubElement(widget, "horizontal_alignment").text = str(1)
    ET.SubElement(widget, "vertical_alignment").text = str(1)


def json_to_xml(json_data, syst):
    """Convert JSON data to XML format."""
    display = ET.Element("display", version="2.0.0")
    ET.SubElement(display, "name").text = f"{syst} systems"

    macros = ET.SubElement(display, "macros")
    ET.SubElement(macros, "SEP").text = ":"

    ET.SubElement(display, "width").text = "2372"
    ET.SubElement(display, "height").text = "978"

    index = 0
    for system, data in json_data.items():
        if syst in system:
            sys_list = data["sys"]
            dln_list = data["dln"]
            scu_list = data["scu"]
            mc_type_list = data["mc_type"]
            slot_list = data["slot"]
            port_list = data["port"]
            for i in range(len(sys_list)):
                sys_val = sys_list[i]
                dln_val = dln_list[i]
                scu_val = scu_list[i]
                mc_type_val = mc_type_list[i]
                slot_val = slot_list[i]
                port_val = port_list[i]
                if index % 27 == 0:
                    initial_x_masked_position = 223
                    create_filter_mask_widget(
                        display, "Masked", initial_x_masked_position, index
                    )
                    initial_x_filtered_position = 283
                    create_filter_mask_widget(
                        display, "Filtered", initial_x_filtered_position, index
                    )
                create_widget(
                    display,
                    sys_val,
                    dln_val,
                    scu_val,
                    mc_type_val,
                    slot_val,
                    port_val,
                    index,
                )
                index += 1

    return ET.tostring(display, encoding="utf-8").decode("utf-8")


def save_xml(xml_str, filename):
    """Save XML string to a file."""
    with open(filename, "w") as file:
        file.write(xml_str)


with open("temp_files/opi_concatenated_systems.json", "r") as json_file:
    json_data = json.load(json_file)


parser = argparse.ArgumentParser(description="OPI for which concatenated system:")
parser.add_argument("system", type=str, help="i.e BLM, BPM, BCM, RFLPS, AptM ...")

args = parser.parse_args()

xml_output = json_to_xml(json_data, args.system)
save_xml(xml_output, f"output_xml/{args.system}.xml")
