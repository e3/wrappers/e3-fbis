import pandas as pd
import argparse
import json
import os


def make_key(line):
    """
    Functions used to generating key for different alias
    """
    if "BCM01" in line or "PBI-BCM-001" in line:
        key = "PBI-BCM01"
        return key
    elif line.startswith("ISrc-LPS"):
        key = "ISrc-LPS:Ctrl-PLC-01"
        return key
    elif line.startswith("LEBT-CS"):
        key = "LEBT-Chop"
        return key
    elif "MEBT-010:BMD-Chop-001" in line:
        key = "MEBT-010:BMD-Chop-001"
        return key
    elif "RFC:" "RFC:" in line or "RFS-FIM" in line:
        parts = line.split(":", 2)
        if len(parts) >= 3:
            prefix = f"{parts[0]}:{parts[1]}"
            suffix = parts[2]
            key = f"{prefix}"
            return key
    elif any(x in line for x in ["PBI-AptM", "PBI-Img", "PBI-Grd"]):
        parts = line.split(":", 2)
        if len(parts) >= 3:
            prefix = f"{parts[0]}:{parts[1]}"
            key = f"{prefix}"
            return key
    elif "Vac-VVF" in line:
        parts = line.split(":", 2)
        if len(parts) >= 3:
            prefix = f"{parts[0]}:{parts[1]}"
            key = f"{prefix}"
            return key
    elif (
        line.startswith("PBI-BPM")
        or line.startswith("PBI-ICBLM")
        or line.startswith("PBI-nBLM")
    ):
        parts = line.split(":", 2)
        if len(parts) >= 3:
            prefix = f"{parts[0]}:{parts[1]}"
            key = f"{prefix}"
            return key
    else:
        split_line = line.split(":", 1)
        if "ISrc-CS" == split_line[0]:
            pass
        else:
            key = split_line[0]
            return key


# Setup argparse to handle command-line arguments
parser = argparse.ArgumentParser(
    description="Extract a column from an Excel sheet and save it to a text file."
)
parser.add_argument("dln_instance", type=str, help="DLN01, DLN02 ...")
args = parser.parse_args()

excel_file = f"../../param_files/Parameterization_Files_Excel/{args.dln_instance}.xlsm"

# Read the specific column from the Excel file
df = pd.read_excel(
    excel_file, sheet_name="DLN_PU", usecols=[1, 3, 4, 5, 7, 13], skiprows=5
)

update_heading = lambda heading: heading.replace("\n", "_").replace(" ", "_")
df.columns = list(map(update_heading, df.columns.values.tolist()))
# Making new column
## Making Port column
port_column = []
json_key = []
slot = []
for i, row in df.iterrows():
    if "RS485MC" == str(row["Associated_MC_Type"]):
        if str(row["Slink_Fast_Frame_Sequence"]) == "True":
            if row["Index_Offset"] < 3:
                port_column.append(1)
            else:
                port_column.append(2)
        else:
            port_column.append(0)
    else:
        port_column.append(0)

    if type(row["Associated_MC_Slot"]) == str:
        slot.append(str(row["Associated_MC_Slot"]).split(" ")[1])
    else:
        slot.append(None)

    json_key.append(make_key(row["Alias"]))

df["MC_Slot"] = slot
df["Port"] = port_column
df["Json_key"] = json_key

df_smaller = df[
    [
        "Associated_SCU",
        "Associated_MC_Slot",
        "Associated_MC_Type",
        "MC_Slot",
        "Port",
        "Json_key",
    ]
].drop_duplicates(subset=["Json_key"])

df_cleaned = df_smaller.dropna()

json_file_name = f"temp_files/opi_{args.dln_instance}.json"
with open(json_file_name, "r") as json_file:
    json_data = json.load(json_file)
    for i, row in df_cleaned.iterrows():
        key = row["Json_key"]
        dic = json_data[key]
        dic["scu"] = row["Associated_SCU"]
        dic["mc_type"] = row["Associated_MC_Type"]
        dic["slot"] = row["MC_Slot"]
        dic["port"] = str(row["Port"])

os.remove(json_file_name)

with open(json_file_name, "w") as json_file:
    json.dump(json_data, json_file, indent=4)
