# Instructions for Creating the OPI for Concatenated Systems

```bash
# Step 1: Run Preliminary Scripts
# To begin, execute the preliminary setup script using the following command:

sh run_preliminary_scripts.sh

# Step 2: Create the OPI for Specific Systems
# Depending on the system you're working with, use one of the following commands to generate the OPI:

# For the BLM system:
python opi_create.py BLM

# For the RFLPS system:
python opi_create.py RFLPS

# Step 3: Locate the Output XML File
#Once the command has executed, an XML file will be generated in the output_xml folder.
#This XML file is the BOB file, which is suitable for use in CSS.
