def format_text(input_text_path, output_text_path):
    with open(input_text_path, "r") as input_file:
        lines = input_file.readlines()

    with open(output_text_path, "w") as output_file:
        for line in lines:
            formatted_line = line.strip()
            output_file.write(
                f'dbLoadRecords("./mask_system.db", "SYS={formatted_line}")\n'
            )


# Example usage
input_text_path = "temp_files/all_systems.txt"
output_text_path = "../../test/server/mask/mask_systems.iocsh"
format_text(input_text_path, output_text_path)
