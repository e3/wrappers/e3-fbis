# Instructions for Creating the OPI for Concatenated Systems

```bash
# Step 1: Run Preliminary Scripts
# To begin, execute the preliminary setup script using the following command:

sh run_preliminary_scripts.sh

# Create the first layer of calcout for compute the PV "FBIS::is_masked_ok"
python mask_create_calc_first_layer.py

# Create the (partial) substition file with all systems
python create_substitution.py

# Create the OPI (the list of systems) with all systems listed
python mask_create_bob_file.py

# Step 3: Locate the Output XML File
#Once the command has executed, an XML file will be generated in the output_xml folder.
#This XML file is the BOB file, which is suitable for use in CSS.
