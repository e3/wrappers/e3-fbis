import json


def json_to_text(input_json_path, output_text_path):
    with open(input_json_path, "r") as json_file:
        data = json.load(json_file)

    with open(output_text_path, "w") as text_file:
        for key in data.keys():
            if ":" in key:
                text_file.write(f"{key}:\n")
            else:
                text_file.write(f"{key}::\n")


# Example usage
input_json_path = "temp_files/mask_systems_signals.json"
output_text_path = "temp_files/all_systems.txt"
json_to_text(input_json_path, output_text_path)
