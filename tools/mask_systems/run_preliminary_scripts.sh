#!/bin/bash

mkdir temp_files
mkdir output_xml

# Run from_excel_to_txt_dln_pus.py for each DLN
for dln in DLN01 DLN02 DLN03 DLN04 DLN05 DLN06 DLN07; do
    python from_excel_to_txt_dln_pus.py $dln
    echo "Completed $dln for from_excel_to_txt_dln_pus.py"
done

# Run from_txt_to_json.py for each DLN
for dln in DLN01 DLN02 DLN03 DLN04 DLN05 DLN06 DLN07; do
    python mask_from_txt_to_json.py $dln
    echo "Completed $dln for from_txt_to_json.py"
done

# Merge the results
python mask_merge.py
echo "Merge process completed."

# Have a txt where all the systems are listed
python from_json_to_txt.py

# Create iocsh file for server
python create_iocsh_for_server.py

echo "Finished"
