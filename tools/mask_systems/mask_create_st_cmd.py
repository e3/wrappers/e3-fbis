def convert_to_db_load_records(input_text_path, output_text_path):
    with open(input_text_path, "r") as input_file:
        lines = input_file.readlines()

    with open(output_text_path, "w") as output_file:
        for line in lines:
            system_name = line.strip()
            # Format each line in the desired `dbLoadRecords` syntax
            output_file.write(
                f'dbLoadRecords("mask_system_to_mask.db", "P={system_name}")\n'
            )

    print(f"Formatted output saved to {output_text_path}")


# Example usage
input_text_path = "temp_files/all_systems.txt"
output_text_path = "temp_files/st_cmd.txt"
convert_to_db_load_records(input_text_path, output_text_path)
