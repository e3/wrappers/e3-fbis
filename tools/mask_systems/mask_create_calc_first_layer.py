def format_records(input_text_path, output_text_path):
    with open(input_text_path, "r") as input_file:
        lines = input_file.readlines()

    with open(output_text_path, "w") as output_file:
        fields_per_record = 12
        total_lines = len(lines)
        record_count = total_lines // fields_per_record

        for record_index in range(record_count):
            start_index = record_index * fields_per_record
            end_index = start_index + fields_per_record

            output_file.write(
                f'record (calc, "FBIS::#is_masked_ok_c_{record_index + 1:03}")\n{{\n'
            )

            for i in range(start_index, min(end_index, total_lines)):
                formatted_line = lines[i].strip()
                output_file.write(
                    f'  field(INP{chr(65 + (i - start_index))}, "{formatted_line}FBIS_mask_mismatch CPP")\n'
                )

            calc_field = " AND ".join(
                [f"{chr(65 + j)}" for j in range(fields_per_record)]
            )
            output_file.write(f'  field(CALC, "{calc_field}")\n')
            output_file.write("}\n\n")


input_text_path = "temp_files/all_systems.txt"
output_text_path = "temp_files/calc.db"
format_records(input_text_path, output_text_path)
