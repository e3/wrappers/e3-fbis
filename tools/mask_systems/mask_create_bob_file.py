import xml.etree.ElementTree as ET


def generate_xml(input_text_path, output_xml_path):
    display = ET.Element("display", version="2.0.0")

    # Add general display metadata elements
    ET.SubElement(display, "name").text = "list systems (mask)"
    ET.SubElement(display, "width").text = "600"
    ET.SubElement(display, "height").text = "2360"

    background_color = ET.SubElement(display, "background_color")
    ET.SubElement(
        background_color, "color", red="220", green="225", blue="221", alpha="0"
    )

    with open(input_text_path, "r") as input_file:
        lines = input_file.readlines()

    for index, line in enumerate(lines):
        system_name = line.strip()

        widget = ET.SubElement(display, "widget", type="embedded", version="2.0.0")
        ET.SubElement(widget, "name").text = f"Embedded Display_{index}"

        macros = ET.SubElement(widget, "macros")
        ET.SubElement(macros, "SYS").text = system_name

        ET.SubElement(widget, "file").text = "mask_widget.bob"
        ET.SubElement(widget, "y").text = str(30 * (index % 80))
        ET.SubElement(widget, "x").text = str(620 * int(index / 80))
        ET.SubElement(widget, "width").text = "600"
        ET.SubElement(widget, "height").text = "30"
        ET.SubElement(widget, "resize").text = "2"

    tree = ET.ElementTree(display)
    tree.write(output_xml_path, encoding="UTF-8", xml_declaration=True)


# Example usage
input_text_path = "temp_files/all_systems.txt"
output_xml_path = "output_xml/output.xml"
generate_xml(input_text_path, output_xml_path)
