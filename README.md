
# e3-fbis

This module is heavily linked e3-dln. This module is used to create PVs for operator OPIs upon PVs bolonging to IOCs based on e3-dln modules.

###  Clone (Initial Checkout)
The e3-fbis makes use of git-submodules for custom template files and to generate PVs that get/set PVs from/to dln IOCs.
```bash
git submodule update --init --recursive
git submodule update --recursive --remote
```

## Installation in cellmode

```sh
$ make cellinstall
```

For further targets, type `make`.

## Usage

```sh
$ iocsh cmds/fbis_xxx.cmd -l cellinstall
```

## About

EPICS IOCs for DLNs are primarily hardware-oriented, meaning they are mainly accessible to system owners and experts. To better serve operators, a more user-friendly system was needed. This led to the development of the concept of an input system. For instance, in the FBIS (Feedback Input System), the input systems include PSS, MPSVac, Isrc, BPM, and others.

Taking PSS as an example, FBIS has four inputs related to PSS: PSS, Target Beam Permit, and Redundant Beam Permit. We need a way to consolidate these four signals. This would allow operators to easily verify that PSS is functioning correctly if all inputs are in good condition. If operators need to mask PSS, they would request a system expert to mask all four inputs. Additionally, since FBIS is redundant, operators should be able to confirm that PSS is functioning correctly if the four inputs are read by two serializers (in the SCU) and processed by two separate IFC1410 boards (in the DLN).

During each commissioning phase, FBIS undergoes configuration changes, often incorporating additional sensor systems. This means that high-level process variables (PVs) used by operators might need adjustments, either by adding more parameters or changing the PVs used in calculations. To facilitate these updates, a set of Python scripts has been created to streamline the process.

### Implementation

To address the need for a modular and scalable system that adapts to different system types and commissioning phases, we developed a structured approach using iocsh, substitution, and template files for each system.

Here’s how it works:

- Loading PVs: For each subsystem, we use iocsh files to load all process variables (PVs) for each redundant channel (A and B). If the system is concatenated, we concatenate the PVs of the two channels.

- Logic Operations: Each system or subsystem performs a logical AND operation across all its signals. For example, in the case of PSS, this involves ANDing four signals. This approach is applied to input, masking, filtering, latching, and output status PVs. To streamline this process, we use templates and substitution files.

- Redundancy Check: We need to ensure that the redundant signals of each system or subsystem match. Therefore, we create PVs to indicate whether each system or subsystem is operating normally or if it is in a BI (Bad Input) or RBI (Redundant Bad Input) state.

 - Concatenated Systems: For concatenated systems (such as RF, BCM, BPM, and BLM), additional care is taken to manage the concatenation between all the subsystems.

This approach can be summarized with a kind of UML diagram, which illustrates how the files are organized for systems like RF (a concatenated system).

![RF_diagram](./docs/img/rf_diagram.png)

## Test
How to test the e3 module is described in details in the [README](./test/README.md) in test folder.

## Tools
In the [README](./tools/README.md) there are more details on how to use scripts convenient to generate PVs, OPIs and other files.

## Software environment setup

To set up the software environment for testing and running the tools script, follow these steps:

1. Create a virtual environment: ```python3.9 -m venv ~/xxx ```
2. Activate the virtual environment: ```source ~/xxx/bin/activate```
3. Upgrade pip: ```pip install -U pip```
4. Install the required packages: ```pip install -r requirements.txt```
