#include <stdio.h>
#include <inttypes.h>
#include <math.h>
#include <strings.h>
#include <dbDefs.h>
#include <subRecord.h>
#include <dbCommon.h>
#include <recSup.h>
#include <registryFunction.h>
#include <epicsExport.h>
#include <epicsTypes.h>
#include <cantProceed.h>
#include <registryFunction.h>
#include <epicsExport.h>
#include <aSubRecord.h>
#include <epicsStdlib.h>
#include <epicsStdio.h>

#include <string.h>

static long inhibit_interlock(aSubRecord *prec)
{
  uint8_t *_latch_n  = (uint8_t*)prec->a;
  uint8_t *_dlv_ok   = (uint8_t*)prec->b;

  uint8_t latch_n  = _latch_n[0];
  uint8_t dlv_ok   = _dlv_ok[0];

  if (dlv_ok != 0){ // if OK
    *(uint8_t *)prec->vala = 1; // No BI
    *(uint8_t *)prec->valb = 1; // No RBI
  } else {
    if (latch_n != 0){ // if system not latched and NOK
      *(uint8_t *)prec->vala = 0; // BI asserted
      *(uint8_t *)prec->valb = 1; // No RBI
    }
    else{
      *(uint8_t *)prec->vala = 1; // No BI
      *(uint8_t *)prec->valb = 0; // RBI asserted
    }
  }
  return 0;
}

static long scu_status(aSubRecord *prec)
{
  uint8_t *_a  = (uint8_t*)prec->a;
  uint8_t *_b   = (uint8_t*)prec->b;

  uint8_t a  = _a[0];
  uint8_t b  = _b[0];

  if ((a ==  0) && (b ==  0))
      *(uint8_t *)prec->vala = 0; // NOK
  else if ((a ==  0) && (b ==  1) )
      *(uint8_t *)prec->vala = 2; // Undefined
  else if ((a ==  1) && (b ==  0) )
      *(uint8_t *)prec->vala = 2; // Undefined
  else if ((a ==  1) && (b ==  1) )
      *(uint8_t *)prec->vala = 1; // OK
  else if ((a ==  1) && (b ==  3) )
      *(uint8_t *)prec->vala = 3; // HB software running
  else if ((a ==  3) && (b ==  1) )
      *(uint8_t *)prec->vala = 3; // HB software running
  else
      *(uint8_t *)prec->vala = 0;

  return 0;
}


epicsRegisterFunction(inhibit_interlock);
epicsRegisterFunction(scu_status);
