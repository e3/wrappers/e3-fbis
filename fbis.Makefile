#
#  Copyright (c) 2019    European Spallation Source ERIC
#
#  The program is free software: you can redistribute
#  it and/or modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation, either version 2 of the
#  License, or any newer version.
#
#  This program is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
#  more details.
#
#  You should have received a copy of the GNU General Public License along with
#  this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
#
#
# Author  : Stefano Pavinato
# email   : stefano.pavinato@ess.eu
# version : 1.0.0

## The following lines are mandatory, please don't change them.
where_am_I := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
include $(E3_REQUIRE_TOOLS)/driver.makefile


EXCLUDE_ARCHS += linux-ppc64e6500


APPDB:=Db
APPSRC:=src

USR_INCLUDES += -I$(where_am_I)$(APPSRC)


DBDINC_DBDS = $(subst .c,.dbd,   $(DBDINC_SRCS:$(APPSRC)/%=%))
DBDINC_HDRS = $(subst .c,.h,     $(DBDINC_SRCS:$(APPSRC)/%=%))
DBDINC_DEPS = $(subst .c,$(DEP), $(DBDINC_SRCS:$(APPSRC)/%=%))

#SUBS=$(wildcard $(APPDB)/in_system_*.substitutions)
#TMPS= $(APPDB)/inp_system_generic.template
#TEMPLATES += $(wildcard $(APPDB)/input_system_*.substitutions)

HEADERS += $(DBDINC_HDRS)

SOURCES += $(APPSRC)/subs.c
# DBDINC_SRCS should be last of the series of SOURCES
SOURCES += $(DBDINC_SRCS)

DBDS += $(APPSRC)/subs.dbd

.PHONY: $(DBDINC_DEPS) .dbd.h
$(DBDINC_DEPS): $(DBDINC_HDRS)

$(DBDINC_HDRS): $(DBDINC_DBDS)
	$(DBTORECORDTYPEH)  $(USR_DBDFLAGS) -o $@ $<


SCRIPTS += $(wildcard ../iocsh/*.iocsh)

USR_DBFLAGS += -I . -I ..
USR_DBFLAGS += -I $(EPICS_BASE)/db
USR_DBFLAGS += -I $(APPDB)

SUBS=$(wildcard $(APPDB)/*.substitutions)
TMPS=$(wildcard $(APPDB)/*.template)

.PHONY: vlibs
vlibs:
